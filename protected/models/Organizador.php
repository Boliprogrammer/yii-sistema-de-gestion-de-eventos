<?php

/**
 * This is the model class for table "t_organizador".
 *
 * The followings are the available columns in table 't_organizador':
 * @property string $idt_organizador
 * @property string $organizador_nombre
 * @property string $organizador_apellido
 * @property string $organizador_empresa
 * @property string $organizador_correo
 * @property string $organizador_clave
 * @property string $create_time //organizador_creacion
 * @property string $update_time //organizador_modificacion
 */
class Organizador extends TesisActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Organizador the static model class
	 */
	 
	public $organizador_clave_repeat;
	public $verifyCode;
	
	protected function afterValidate()
	{
		parent::afterValidate();
		$this->organizador_clave = $this->encrypt($this->organizador_clave);
	}
		
	public function encrypt($value)
	{
		return md5($value);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_organizador';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('organizador_clave', 'compare'),
			array('organizador_correo','unique'),
			array('organizador_correo','email'),
			array('organizador_nombre, organizador_apellido, organizador_correo, organizador_clave, organizador_creacion, organizador_modificacion, organizador_clave_repeat', 'required'),
			array('organizador_nombre, organizador_apellido', 'length', 'max'=>100),
			array('organizador_empresa, organizador_correo', 'length', 'max'=>150),
			array('organizador_clave', 'length', 'max'=>50),
			array('organizador_clave_repeat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_organizador, organizador_nombre, organizador_apellido, organizador_empresa, organizador_correo, organizador_clave, organizador_creacion, organizador_modificacion', 'safe', 'on'=>'search'),
			array('organizador_nombre, organizador_apellido, organizador_empresa, organizador_correo, organizador_clave, organizador_clave_repeat', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('organizador_clave','ext.validators.EPasswordStrength','min'=>8,'message'=>Yii::t('category','Clave muy debil; al menos 8 caracteres que contengan mayusuculas, minusculas y al menos un numero')),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(),'on'=>'insert'),
			//array(''
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactos' => array(self::HAS_MANY, 'Contacto', 'contacto_idt_organizador'),
			'eventos' => array(self::HAS_MANY, 'Evento', 'evento_idt_organizador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_organizador' => 'Organizador',
			'organizador_nombre' => 'Nombre',
			'organizador_apellido' => 'Apellido',
			'organizador_empresa' => 'Empresa',
			'organizador_correo' => 'Correo',
			'organizador_clave' => 'Clave',
			'organizador_creacion' => 'Create Time',
			'organizador_modificacion' => 'Update Time',
			'verifyCode'=>'Codigo de verificacion'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_organizador',$this->idt_organizador,true);
		$criteria->compare('organizador_nombre',$this->organizador_nombre,true);
		$criteria->compare('organizador_apellido',$this->organizador_apellido,true);
		$criteria->compare('organizador_empresa',$this->organizador_empresa,true);
		$criteria->compare('organizador_correo',$this->organizador_correo,true);
		$criteria->compare('organizador_clave',$this->organizador_clave,true);
		$criteria->compare('organizador_creacion',$this->organizador_creacion,true);
		$criteria->compare('organizador_modificacion',$this->organizador_modificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	//filterUsuario() Fernando Villarroel
	//$obj->search()->Usuario()
}
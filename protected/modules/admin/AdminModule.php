<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
			'admin.modules.*',
			'application.modules.eventos.models.Participante',
			'application.modules.eventos.modules.shop.models.Order',
			'application.modules.eventos.modules.shop.models.OrderPosition',
			//'application.modules.eventos.modules.shop.models.Shop',
			//'application.modules.eventos.controllers.EventoController',
			//'application.extensions.FixedCheckboxColumn',
		));
		
		//Login basado en m�dulo
		/*
		$this->setComponents(array(
            'errorHandler' => array(
                'errorAction' => 'admin/default/error'),
            'user' => array(
                'class' => 'CWebUser',             
                'loginUrl' => Yii::app()->createUrl('admin/default/login'),
            )
        ));
 
		//Yii::app()->user->setStateKeyPrefix('_organizador');
		Yii::app()->user->setStateKeyPrefix('_admin');
		*/
		//$this->layout = 'main';
		
		//$this->layoutPath;
		
		//$this->layoutPath = Yii::getPathOfAlias('application.modules.admin.views.layouts');
        //$this->layout = '/layouts/main';
		
		//$this->layout = 'admin';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			// this overwrites everything in the controller
			//$controller->layout = 'admin';
			/*$controller->layout = 'main';
			$controller->layout = 'column1';
			$controller->layout = 'column2';*/
			
			//Otra alternativa para redireccionar al m�dulo respectivo
			//if(Yii::app()->getModule('admin')->user->isGuest)
				//Yii::app()->getModule('admin')->user->setReturnUrl('admin/default/login');
				
			/* Module Based Login
			//Redireccionar al m�dulo respectivo
			$route = $controller->id . '/' . $action->id;
            // echo $route;
            $publicPages = array(
                'default/login',
                'default/error',
            );
			
            if (Yii::app()->user->isGuest && !in_array($route, $publicPages)){            
                Yii::app()->getModule('admin')->user->loginRequired();                
            }
            else
			*/
			
			return true;
		}
		else
			return false;
	}
}

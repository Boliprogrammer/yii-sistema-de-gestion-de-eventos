<?php

class CampanhaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/* array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			), */
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'users'=>array('@'),
			),
			/* array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			), */
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//if($model->campanhaEvento->evento_idt_organizador===Yii::app()->user->id){
		if (Yii::app()->user->getState('idt_evento_actual')===null)
			throw new CHttpException(501,'Peticion invalida. Seleccione una campanha.');
		else
		{
			$model = $this->loadModel($id);
			if($model->campanha_idt_evento===Yii::app()->user->getState('idt_evento_actual')){
				$this->render(
					'view',array(
						'model'=>$model,
					)	
				);
				Yii::app()->user->setState('idt_campanha_actual',$id);
				Yii::app()->user->setState('asunto',$model->campanha_asunto);
				Yii::app()->user->setState('mensaje',$model->campanha_mensaje);
			}
			else
				throw new CHttpException(400,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (Yii::app()->user->getState('idt_evento_actual')===null)
			throw new CHttpException(40067,'Selecciona un evento por favor, gracias.');
		else
		{
			$model=new Campanha;

			if(isset($_POST['Campanha']))
			{
				$model->attributes=$_POST['Campanha'];
				$model->campanha_idt_evento = Yii::app()->user->getState('idt_evento_actual');
				if($model->save())
					$this->redirect(array('view','id'=>$model->idt_campanha));
			}

			$this->render('create',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		//if($model->campanhaEvento->evento_idt_organizador===Yii::app()->user->id){
		
		if (Yii::app()->user->getState('idt_evento_actual')===null)
			throw new CHttpException(501,'Peticion invalida. Seleccione una campanha.');
		else
		{
			$model=$this->loadModel($id);
			if($model->campanha_idt_evento===Yii::app()->user->getState('idt_evento_actual'))
			{
				if(isset($_POST['Campanha']))
				{
					$model->attributes=$_POST['Campanha'];
					if($model->save())
						$this->redirect(array('view','id'=>$model->idt_campanha));
				}
				$this->render('update',array('model'=>$model,));
			}
			else
				throw new CHttpException(400,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		}
		

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteriaCondition1 = 'campanha_idt_evento=' .Yii::app()->user->getState('idt_evento_actual');
		$dataProvider=new CActiveDataProvider('Campanha', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
		));
		
		//$dataProvider=new CActiveDataProvider('Campanha');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Campanha('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Campanha']))
			$model->attributes=$_GET['Campanha'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Campanha::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='campanha-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class ContactoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	 
	/* 
	public function filterOrganizador($filterChain)
	{	
		$model=new Contacto;
		if(isset($_GET['Contacto']))
			$model->attributes=$_GET['Contacto'];
		else
			if(isset($_POST['Contacto']))
				$model->attributes=$_POST['Contacto'];
		if($model->contacto_idt_organizador==Yii::app()->user->id){
			throw new CHttpException(404,'No tiene los privilegios para ejecutar este comando');
		}
		$filterChain->run();
	} */
	 
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'organizador + update, view',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete','importar'),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
		
		if($model->contacto_idt_organizador===Yii::app()->user->id){
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		else
			throw new CHttpException(400,'Peticion invalida. Por favor no repita esta peticion otra vez.');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Contacto;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Contacto']))
		{
			$model->attributes=$_POST['Contacto'];
			$model->contacto_idt_organizador= Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('view','id'=>$model->idt_contacto));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->contacto_idt_organizador===Yii::app()->user->id)
		{
			if(isset($_POST['Contacto']))
			{
				$model->attributes=$_POST['Contacto'];
				if($model->save())
					$this->redirect(array('view','id'=>$model->idt_contacto));
			}
		
			$this->render('update',array(
				'model'=>$model,
			));
		}
		else
			throw new CHttpException(400,'Peticion invalida. Por favor no repita esta peticion otra vez.');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteriaCondition1 = 'contacto_idt_organizador=' .Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('Contacto', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
			
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		/*
		$dataProvider=new CActiveDataProvider('Contacto');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Contacto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contacto']))
			$model->attributes=$_GET['Contacto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	/*
	public function actionImportar()
	{
		$this->layout = 'importLayout';
		$criteriaCondition1 = 'contacto_idt_organizador=' .Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('Contacto', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
			'pagination'=>array(
				'pageSize' => 2,
			),
		));
		$this->render('importarForm', compact('dataProvider'));
	}*/
	
	//Importar sin el selGridView
	public function actionImportar()
	{
				
		$model=new Contacto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contacto']))
			$model->attributes=$_GET['Contacto'];
		
		$this->render('importarContactos',array(
			'model'=>$model,
		));
	
		/* //
		$this->layout = 'importLayout';
		
		$model=new Contacto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contacto']))
			$model->attributes=$_GET['Contacto'];
		
		$this->render('importarForm',array(
			'model'=>$model,
		));
		*/
		
		/*
		$criteriaCondition1 = 'contacto_idt_organizador=' .Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('Contacto', array(
			'pagination' => array(
				'pageSize' => 2,
			),
			'criteria'=>array('condition'=>$criteriaCondition1,),
			
		));
		$this->render('importarForm', compact('dataProvider'));
		*/
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Contacto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La pagina solicitada no existe.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contacto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// funci�n sin usar ->
	public function actionImportarAgenda()
	{
		$model=new Contacto('listar');

		// uncomment the following code to enable ajax-based validation
		/*
		if(isset($_POST['ajax']) && $_POST['ajax']==='contacto-importarAgenda-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		*/

		if(isset($_POST['Contacto']))
		{
			$model->attributes=$_POST['Contacto'];
			if($model->validate())
			{
				// form inputs are valid, do something here
				return;
			}
		}
		$this->render('importarAgenda',array('model'=>$model));
	} 
}

<?php

class EventoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'users'=>array('*'),
			), */
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','buscarParticipantes','delete'),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
		if($model->evento_idt_organizador===Yii::app()->user->id){
			Yii::app()->user->setState('idt_evento_actual',$id);
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		else
			throw new CHttpException(40023,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	public function actionCreate()
	{
		Yii::import('ext.multimodelform.MultiModelForm');
	 
		$model = new Evento;
		//$memberCero = new Member;
		
		$entrada = new Entrada;
		$validatedEntradas = array();  //ensure an empty array
		$deleteEntradas = array(); //para permitir que se cree con la foreign key from: //http://www.yiiframework.com/forum/index.php/topic/20289-extension-multimodelformjqrelcopy/page__st__40 
		
	 
		//if($model->evento_idt_organizador===Yii::app()->user->id)
		//{
			if(isset($_POST['Evento'], $_POST['Entrada']))
			{
				$model->attributes=$_POST['Evento'];
				$entrada->attributes=$_POST['Entrada'];
				$model->evento_idt_organizador= Yii::app()->user->id;
				//$memberCero->attributes=$_POST['Member'];
				//$memberCero->groupid = $model->id;
				
				//$valid=$model->validate();
				//$valid=$memberCero->validate() && $valid;
				
				//build a 'dummy' $masterValues for validation only so that OrderID is not blank
				$masterValues = array('entrada_idt_evento' => 0); 
		 
				//submit the $masterValues on validate too  
				if( //validate detail before saving the master
					MultiModelForm::validate($entrada,$validatedEntradas,$deleteItems,$masterValues) && 
					$model->save() /*&& $memberCero->save() */
				   )
					{
						//the value for the foreign key 'groupid'
						$masterValues = array ('entrada_idt_evento'=>$model->idt_evento);
						if (MultiModelForm::save($entrada,$validatedEntradas,$deleteEntradas,$masterValues))
							$this->redirect(array('view','id'=>$model->idt_evento));
					}
			}
		 
			$this->render('create',array(
				'model'=>$model,
				/*'memberCero' => $member,*/
				//submit the member and validatedItems to the widget in the edit form
				'entrada'=>$entrada,
				'validatedEntradas' => $validatedEntradas,
			));
		/* }
		else
			throw new CHttpException(400,'Peticion invalida. Por favor no repita esta peticion otra vez.'); */
	}	
	/*
	public function actionCreate()
	{
		$model=new Evento;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		
		if(isset($_POST['Evento']))
		{
			$model->attributes=$_POST['Evento'];
			$model->evento_idt_organizador= Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('view','id'=>$model->idt_evento));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	*/
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		Yii::import('ext.multimodelform.MultiModelForm');
		
		
		$model=$this->loadModel($id);
		$entrada = new Entrada;
		$validatedEntradas = array();  //ensure an empty array
		$deleteEntradas = array(); //para permitir que se cree con la foreign key from: //http://www.yiiframework.com/forum/index.php/topic/20289-extension-multimodelformjqrelcopy/page__st__40 
	
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		/* if(isset($_POST['Evento']))
		{
			$model->attributes=$_POST['Evento'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idt_evento));
		} */

		if($model->evento_idt_organizador===Yii::app()->user->id)
		{
			if(isset($_POST['Evento'], $_POST['Entrada']))
			{
				$model->attributes=$_POST['Evento'];
				//$model->evento_idt_organizador= Yii::app()->user->id;
				//$memberCero->attributes=$_POST['Member'];
				//$memberCero->groupid = $model->id;
				
				//$valid=$model->validate();
				//$valid=$memberCero->validate() && $valid;
				
				//build a 'dummy' $masterValues for validation only so that OrderID is not blank
				$masterValues = array('entrada_idt_evento' => 0); 
		 
				//submit the $masterValues on validate too  
				if( //validate detail before saving the master
					MultiModelForm::validate($entrada,$validatedEntradas,$deleteItems,$masterValues) && 
					$model->save() /*&& $memberCero->save() */
				   )
					{
						//the value for the foreign key 'groupid'
						$masterValues = array ('entrada_idt_evento'=>$model->idt_evento);
						if (MultiModelForm::save($entrada,$validatedEntradas,$deleteEntradas,$masterValues))
							$this->redirect(array('view','id'=>$model->idt_evento));
					}
			}
	
			$this->render(
				'update',array(
					'model'=>$model,
					/*'memberCero' => $member,*/
					//submit the member and validatedItems to the widget in the edit form
					'entrada'=>$entrada,
					'validatedEntradas' => $validatedEntradas,
				)
			);
		}
		else
			throw new CHttpException(40099,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		
		/*$this->render('update',array(
			'model'=>$model,
		));*/
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			//$cuantos = array();
			//$query = "select COUNT(DISTINCT idt_participante) from t_participante, t_shop_order, t_shop_order_position, t_entrada, t_evento where t_evento.idt_evento=t_entrada.entrada_idt_evento and t_shop_order_position.product_id=t_entrada.idt_entrada and t_shop_order_position.order_id=t_shop_order.order_id and t_participante.idt_participante=t_shop_order.customer_id and entrada_idt_evento=72";//.$model->idt_evento;
			//$query = "SELECT COUNT(DISTINCT `t`.`idt_participante`) FROM `t_participante` `t` LEFT OUTER JOIN `t_shop_order` `compras` ON (`compras`.`customer_id`=`t`.`idt_participante`) LEFT OUTER JOIN `t_shop_order_position` `entradas` ON (`entradas`.`order_id`=`compras`.`order_id`) LEFT OUTER JOIN `t_entrada` `entrada` ON (`entradas`.`product_id`=`entrada`.`idt_entrada`) LEFT OUTER JOIN `t_evento` `entradaEvento` ON (`entrada`.`entrada_idt_evento`=`entradaEvento`.`idt_evento`) WHERE (t_evento.idt_evento=72)";
			// we only allow deletion via POST request
			//$dbCommand = Yii::app()->db->createCommand($query);
			
			//$cuantos=$dbCommand->queryScalar();
			
			//if($cuantos<1)
			//{
				$model->delete();
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			//}
				//throw new CHttpException(40019,'No se puede eliminar porque ya tiene participantes para este evento.');
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			//if(!isset($_GET['ajax']))
			//	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(40055,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteriaCondition1 = 'evento_idt_organizador=' .Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('Evento', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		/*
		$dataProvider=new CActiveDataProvider('Evento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Evento('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Evento']))
			$model->attributes=$_GET['Evento'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionBuscarParticipantes()
	{
		//$model=new Evento('search');
		$model=new Participante('buscarParticipante');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Participante']))
			$model->attributes=$_GET['Participante'];

		$this->render('buscarParticipantes',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Evento::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='evento-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

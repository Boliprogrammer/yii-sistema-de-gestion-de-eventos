<?php

class InvitacionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	//public $oculto;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('captcha'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','admin', 'importar'/*, 'update'*/),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	//no ten�a esta funci�n
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if (Yii::app()->user->getState('idt_campanha_actual')===null)
			throw new CHttpException(6011,'Peticion invalida. Seleccione una invitacion.');
		else
		{
			$model = $this->loadModel($id);
			if($model->invitacion_idt_campanha===Yii::app()->user->getState('idt_campanha_actual')){
				$this->render('view',array(
					'model'=>$model,
				));
			}
			else
				throw new CHttpException(40030,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	 
	/* //POR DEFECTO 
	public function actionCreate()
	{
		$model=new Invitacion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invitacion']))
		{
			$model->attributes=$_POST['Invitacion'];
			$model->invitacion_idt_campanha = Yii::app()->user->getState('idt_campanha_actual');
			if($model->save())
				$this->redirect(array('view','id'=>$model->idt_invitacion));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	*/
	
	public function actionCreate()
	{
		$model=new Invitacion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (Yii::app()->user->getState('idt_campanha_actual')===null)
			throw new CHttpException(40044,'Selecciona una campanha por favor, gracias.');
		else
		{
			
			if(array_key_exists('oculto', $_POST))
			{
				$valor = array();
				$valor = $_POST['oculto'];
				$model = new Invitacion;
				$model->invitacion_correos = $valor;
				//$model->idt_invitacion = 0;
				$this->render('create',
					array(
						'model'=>$model,
					)
				);
			}
			else
			{
				if(isset($_POST['Invitacion']))
				{
					$model->attributes=$_POST['Invitacion'];
					$model->invitacion_idt_campanha = Yii::app()->user->getState('idt_campanha_actual');
					if($model->save())
					{
						$headers="From: no-reply@opceventos.com\r\nReply-To: support@opceventos.com";
						//$headers="From: {$model->email}\r\nReply-To: {$model->email}";
						//mail("alejandrobolivar86@gmail.com","Este es el asunto forzado por codigo","Este es el mensaje forzado por codigo para verificar la configuracion del mail server",$headers);
						mail($model->invitacion_correos,Yii::app()->user->getState('asunto'),Yii::app()->user->getState('mensaje'),$headers);
						$this->redirect(array('view','id'=>$model->idt_invitacion));
						
					}
				}

				$this->render('create',
					array(
						'model'=>$model,
					)
				);
			}
		}
	}
	
	// SOLO PRUEBAS
	public function actionUpdate()
	{
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		
		//$model=$this->loadModel($id);
		$valor = array();
		
		$valor = $_POST['oculto'];
		$model = new Invitacion;
		$model->invitacion_correos = $valor;
		$model->idt_invitacion = 0;
		
		//if($model->invitacion_idt_campanha===Yii::app()->user->getState('idt_campanha_actual'))
		//{
			
			if(isset($_POST['Invitacion']))
			{
				throw new CHttpException(0000,'Erro catastrofico');
				/* $model->attributes=$_POST['Invitacion'];
				if($model->save())
					$this->redirect(array('view','id'=>$model->idt_invitacion));
				*/
			}
			
			$this->render('update',array('model'=>$model,));
		//}

	}
	
	// esta funci�n esta implementada en ContactoController. ESTO ES SOLO PARA PRUEBAS
	public function actionImportar()
	{
		$model=new Invitacion;
		$model2 = new Contacto;
		//$valor = $_POST['oculto'];
		//$valor = 'valor XYZ';
		//$valor = @$_POST['Contacto']['oculto'];
		//$valor = $_POST['Contacto']['oculto'];
		//valor = @$_POST['oculto'];
		$valor = array();
		
		$valor = $_POST['oculto'];
		
		/* if(array_key_exists('oculto', $_POST))
			throw new CHttpException(1,'existe');
		else
			throw new CHttpException(0,'no existe');
		*/
		//$cookie=Yii::app()->request->cookies['name'];
		//$value=$cookie->value;
		
		if(isset($valor))
		{
			throw new CHttpException(0001,$valor);
		}
		else
		{
			//throw new CHttpException(0000,'No Tiene valor.');
			throw new CHttpException(0000,$valor);
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		/*$model->invitacion_correos=Yii::app()->user->getState('correosSeleccionados'); */
		
		
		//$value = Yii::app()->request->cookies->contains('the_cookie') ?Yii::app()->request->cookies['the_cookie']->value : '';

		//$model->invitacion_correos=$value;
		//$this->refresh();
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	// no sirve, solo sirve de ejemplo para basarme para enviar invitacion.
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				//$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				$headers="From: no-reply@opceventos.com\r\nReply-To: support@opceventos.com";
				//mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				//mail($model->email,$model->subject,$model->body,$headers);
				mail($model->email,Yii::app()->user->getState('asunto'),Yii::app()->user->getState('mensaje'),$headers);
				Yii::app()->user->setFlash('contact','Su Campanha ha sido enviada con exito.');
				$this->refresh();
			}
		}
		
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	 
	 // no se puede actualizar un env�o porque algo que se envi� y ya pas�.


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	 
	 //no se puede eliminar un env�o porque ya se envi� y ya pas�.
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(40093,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		$criteriaCondition1 = 'invitacion_idt_campanha=' .Yii::app()->user->getState('idt_campanha_actual');
		$dataProvider=new CActiveDataProvider('Invitacion', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
		));
	
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		
		/* //c�digo por defecto
		$dataProvider=new CActiveDataProvider('Invitacion');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invitacion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invitacion']))
			$model->attributes=$_GET['Invitacion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Invitacion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invitacion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

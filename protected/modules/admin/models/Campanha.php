<?php

/**
 * This is the model class for table "t_campanha".
 *
 * The followings are the available columns in table 't_campanha':
 * @property string $idt_campanha
 * @property string $campanha_nombre
 * @property string $campanha_asunto
 * @property string $campanha_mensaje
 * @property string $campanha_idt_evento
 *
 * The followings are the available model relations:
 * @property Evento $campanhaIdtEvento
 */
class Campanha extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Campanha the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_campanha';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('campanha_nombre, campanha_asunto', 'required'),
			array('campanha_nombre', 'unique', 'criteria'=>array(
				'condition'=>'`campanha_idt_evento`=:idEventoActual',
				'params'=>array(
					':idEventoActual'=>Yii::app()->user->getState('idt_evento_actual')
				)
			)),
			array('campanha_nombre, campanha_asunto', 'length', 'max'=>200),
			array('campanha_idt_evento', 'length', 'max'=>10),
			array('campanha_mensaje', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_campanha, campanha_nombre, campanha_asunto, campanha_mensaje', 'safe', 'on'=>'search'),
			array('campanha_nombre, campanha_asunto, campanha_mensaje', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'campanhaEvento' => array(self::BELONGS_TO, 'Evento', 'campanha_idt_evento'),
			'invitacion' => array(self::HAS_MANY, 'Invitacion', 'invitacion_idt_campanha'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_campanha' => 'Campanha',
			'campanha_nombre' => 'Nombre',
			'campanha_asunto' => 'Asunto',
			'campanha_mensaje' => 'Mensaje',
			'campanha_idt_evento' => 'Idt Evento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_campanha',$this->idt_campanha,true);
		$criteria->compare('campanha_nombre',$this->campanha_nombre,true);
		$criteria->compare('campanha_asunto',$this->campanha_asunto,true);
		$criteria->compare('campanha_mensaje',$this->campanha_mensaje,true);
		$criteria->compare('campanha_idt_evento',$this->campanha_idt_evento,true);
		$criteriaCondition1 = 'campanha_idt_evento='.Yii::app()->user->getState('idt_evento_actual');
		
		$criteria->addCondition($criteriaCondition1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			/*'criteria'=>array(
				'condition'=>'`campanha_idt_evento`=:idEventoActual',
				'params'=>array(
					':idEventoActual'=>Yii::app()->user->getState('idt_evento_actual'),
				),
			),*/
		));
	}
}
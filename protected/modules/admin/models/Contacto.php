<?php

/**
 * This is the model class for table "t_contacto".
 *
 * The followings are the available columns in table 't_contacto':
 * @property string $idt_contacto
 * @property string $contacto_nombre
 * @property string $contacto_apellido
 * @property string $contacto_correo
 * @property string $contacto_idt_organizador
 *
 * The followings are the available model relations:
 * @property Organizador $contactoIdtOrganizador
 */
class Contacto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contacto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_contacto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contacto_nombre, contacto_apellido, contacto_correo, contacto_idt_organizador', 'required'),
			array('contacto_correo', 'unique', 'criteria'=>array(
				'condition'=>'`contacto_idt_organizador`=:idOrganizadorActual',
				'params'=>array(
					':idOrganizadorActual'=>Yii::app()->user->id
				)
			)),
			array('contacto_nombre, contacto_apellido', 'length', 'max'=>100),
			array('contacto_correo', 'length', 'max'=>150),
			array('contacto_correo', 'email'),
			array('contacto_idt_organizador', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_contacto, contacto_nombre, contacto_apellido, contacto_correo, contacto_idt_organizador', 'safe', 'on'=>'search'),
			array('contacto_nombre, contacto_apellido, contacto_correo', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactoIdtOrganizador' => array(self::BELONGS_TO, 'Organizador', 'contacto_idt_organizador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_contacto' => 'Idt Contacto',
			'contacto_nombre' => 'Nombre',
			'contacto_apellido' => ' Apellido',
			'contacto_correo' => 'Correo',
			'contacto_idt_organizador' => 'Contacto Idt Organizador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_contacto',$this->idt_contacto,true);
		$criteria->compare('contacto_nombre',$this->contacto_nombre,true);
		$criteria->compare('contacto_apellido',$this->contacto_apellido,true);
		$criteria->compare('contacto_correo',$this->contacto_correo,true);
		$criteria->compare('contacto_idt_organizador',$this->contacto_idt_organizador,true);

		$criteriaCondition1 = 'contacto_idt_organizador=' .Yii::app()->user->id;
		$criteria->addCondition($criteriaCondition1);
		
		return new CActiveDataProvider($this, array(
			'keyAttribute' => 'contacto_correo',
			'criteria'=>$criteria,
			//'criteria' => array('condition' => $criteriaCondition1,),
			'pagination' => array(
				'pageSize' => 10,
			),
		));
	}
}
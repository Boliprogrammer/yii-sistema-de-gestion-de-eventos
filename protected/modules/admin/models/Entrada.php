<?php

/**
 * This is the model class for table "t_entrada".
 *
 * The followings are the available columns in table 't_entrada':
 * @property string $idt_entrada
 * @property string $entrada_nombre
 * @property double $entrada_precio
 * @property string $entrada_cantidadMaxima
 * @property string $entrada_idt_evento
 *
 * The followings are the available model relations:
 * @property Evento $entradaIdtEvento
 */
class Entrada extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Entrada the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_entrada';
		//return Shop::module()->productsTable;
	}
	
	public function beforeValidate() {
		if(Yii::app()->language == 'de')
			$this->entrada_precio = str_replace(',', '.', $this->entrada_precio);
		
		return parent::beforeValidate();
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		
		/*return array(
			array('title, category_id', 'required'),
			array('product_id, category_id', 'numerical', 'integerOnly'=>true),
			array('title, price, language', 'length', 'max'=>45),
			array('description, specifications', 'safe'),
			array('product_id, title, description, price, category_id', 'safe', 'on'=>'search'),
		);*/
		return array(
			array('entrada_nombre, entrada_precio, entrada_cantidadMaxima, entrada_idt_evento', 'required'),
			array('entrada_precio', 'numerical'),
			array('entrada_cantidadMaxima', 'numerical', 'integerOnly'=>true),
			array('entrada_nombre', 'length', 'max'=>150),
			array('entrada_cantidadMaxima, entrada_idt_evento', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_entrada, entrada_nombre, entrada_precio, entrada_cantidadMaxima, entrada_idt_evento', 'safe', 'on'=>'search'),
			array('entrada_nombre', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'variations' => array(self::HAS_MANY, 'ProductVariation', 'idt_entrada', 'order' => 'position'),
			'orders' => array(self::MANY_MANY, 'Order', 'ShopProductOrder(order_id, product_id)'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'tax' => array(self::BELONGS_TO, 'Tax', 'tax_id'),
			/*'images' => array(self::HAS_MANY, 'Image', 'product_id'),*/
			/// edición mia
			'images' => array(self::HAS_MANY, 'Image', 'entrada_id'),
			/// fin edición mia
			'shopping_carts' => array(self::HAS_MANY, 'ShoppingCart', 'product_id'),
			//las mias:
			'entradaEvento' => array(self::BELONGS_TO, 'Evento', 'entrada_idt_evento'),
			'entradaTaxId' => array(self::BELONGS_TO, 'Tax', 'entrada_tax_id'),
		);
	}
	
	public function getSpecification($spec) {
		$specs = json_decode($this->specifications, true);

		if(isset($specs[$spec]))
			return $specs[$spec];

		return false;
	}
	
	public function getImage($image = 0, $thumb = false) {
		if(isset($this->images[$image]))
			return Yii::app()->controller->renderPartial('/image/view', array(
				'model' => $this->images[$image]	,
				'thumb' => $thumb), true); 
	}
	
	/* public function getSpecifications() {
		$specs = json_decode($this->specifications, true);
		return $specs === null ? array() : $specs;
	} */
	
	public function setSpecification($spec, $value) {
		$specs = json_decode($this->specifications, true);

		$specs[$spec] = $value;

		return $this->specifications = json_encode($specs);
	}

	public function setSpecifications($specs) {
		foreach($specs as $k => $v)
			$this->setSpecification($k, $v);
	}

	public function setVariations($variations) {
		$db = Yii::app()->db;
		$db->createCommand()->delete('shop_product_variation',
				'product_id = :product_id', array(
					':product_id' => $this->product_id));

		foreach($variations as $key => $value) {
			if($value['specification_id'] 
					&& isset($value['title']) 
					&& $value['title'] != '') {

				if(isset($value['sign']) && $value['sign'] == '-')
					$value['price_adjustion'] -= 2 * $value['price_adjustion'];


				$db->createCommand()->insert('shop_product_variation', array(
							'product_id' => $this->product_id,
							'specification_id' => $value['specification_id'],
							'position' => @$value['position'] ?: 0,
							'title' => $value['title'],
							'price_adjustion' => @$value['price_adjustion'] ?: 0,
							));	
			}
		}
	}

	/*public function getVariations() {
		$variations = array();
		foreach($this->variations as $variation) {
			$variations[$variation->specification_id][] = $variation;
		}		

		return $variations;
	}*/

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		/* return array(
			'idt_entrada' => Yii::t('ShopModule.shop', 'Product'),
			'entrada_nombre' => Yii::t('ShopModule.shop', 'Title'),
			//'description' => Yii::t('ShopModule.shop', 'Description'),
			'entrada_precio' => Yii::t('ShopModule.shop', 'Price'),
			'category_id' => Yii::t('ShopModule.shop', 'Category'),
		); */
		
		return array(
			'idt_entrada' => 'Entrada',
			'entrada_nombre' => 'Nombre de la entrada',
			'entrada_precio' => 'Precio',
			'entrada_cantidadMaxima' => 'Cantidad maxima',
			'entrada_idt_evento' => 'Entrada Idt Evento',
		);
	}
	
	public function getTaxRate($variations = null, $amount = 1) { 
		if($this->entradaTaxId) {
			$taxrate = $this->entradaTaxId->percent;	
			$price = (float) $this->entrada_precio;
			if($variations)
				foreach($variations as $key => $variation) {
					$price += @ProductVariation::model()->findByPk($variation[0])->price_adjustion;
				}


			(float) $price *= $amount;

			(float) $entradaTaxId = $price * ($taxrate / 100);

			return $entradaTaxId;
		}
	}
	
	public function getPrice($variations = null, $amount = 1) {
		$price = (float) $this->entrada_precio;
		if($variations)
			foreach($variations as $key => $variation) {
				$price += @ProductVariation::model()->findByPk($variation[0])->price_adjustion;
			}


		(float) $price *= $amount;

		return $price;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		// by default, lo que vino en el Shop Extension
		/*
		$criteria=new CDbCriteria;

		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider('Products', array(
			'criteria'=>$criteria,
		));
		*/
		
		// lo mio:
		$criteria=new CDbCriteria;

		$criteria->compare('idt_entrada',$this->idt_entrada,true);
		$criteria->compare('entrada_nombre',$this->entrada_nombre,true);
		$criteria->compare('entrada_precio',$this->entrada_precio);
		$criteria->compare('entrada_cantidadMaxima',$this->entrada_cantidadMaxima,true);
		$criteria->compare('entrada_idt_evento',$this->entrada_idt_evento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

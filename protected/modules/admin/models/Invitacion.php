<?php

/**
 * This is the model class for table "t_invitacion".
 *
 * The followings are the available columns in table 't_invitacion':
 * @property string $idt_invitacion
 * @property string $invitacion_correos
 * @property integer $invitacion_cantidad
 * @property integer $invitacion_resultado
 * @property string $invitacion_idt_campanha
 *
 * The followings are the available model relations:
 * @property Campanha $invitacionIdtCampanha
 */
class Invitacion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invitacion the static model class
	 */
	 
	public $verifyCode; 
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_invitacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invitacion_correos, invitacion_idt_campanha', 'required'),
			array('invitacion_cantidad, invitacion_resultado', 'numerical', 'integerOnly'=>true),
			array('invitacion_idt_campanha', 'length', 'max'=>10),
			//array('invitacion_correos','email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_invitacion, invitacion_correos, invitacion_cantidad, invitacion_resultado, invitacion_idt_campanha', 'safe', 'on'=>'search'),
			array('invitacion_correos', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(),'on'=>'insert'),
			array('invitacion_correos', 'application.extensions.MultiEmailValidator.MultiEmailValidator', 'delimiter'=>',', 'min'=>1, 'max'=>1000),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invitacionIdtCampanha' => array(self::BELONGS_TO, 'Campanha', 'invitacion_idt_campanha'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_invitacion' => 'Idt Invitacion',
			'invitacion_correos' => 'Correos',
			'invitacion_cantidad' => 'Invitacion Cantidad',
			'invitacion_resultado' => 'Invitacion Resultado',
			'invitacion_idt_campanha' => 'Invitacion Idt Campanha',
			'verifyCode'=>'Codigo de verificacion'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_invitacion',$this->idt_invitacion,true);
		$criteria->compare('invitacion_correos',$this->invitacion_correos,true);
		$criteria->compare('invitacion_cantidad',$this->invitacion_cantidad);
		$criteria->compare('invitacion_resultado',$this->invitacion_resultado);
		$criteria->compare('invitacion_idt_campanha',$this->invitacion_idt_campanha,true);
		$criteriaCondition1 = 'invitacion_idt_campanha='.Yii::app()->user->getState('idt_campanha_actual');
		$criteria->addCondition($criteriaCondition1);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "t_participante".
 *
 * The followings are the available columns in table 't_participante':
 * @property string $idt_participante
 * @property string $participante_nombre
 * @property string $participante_apellido
 * @property string $participante_correo
 * @property string $participante_clave
 * @property string $participante_creacion
 * @property string $participante_modificacion
 *
 * The followings are the available model relations:
 * @property Compra[] $compras
 */
class Participante extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Participante the static model class
	 */
	public $participante_clave_repeat;
	
	protected function afterValidate()
	{
		parent::afterValidate();
		$this->participante_clave = $this->encrypt($this->participante_clave);
	}
		
	public function encrypt($value)
	{
		return md5($value);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_participante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('participante_clave', 'compare'),
			array('participante_correo', 'unique'),
			array('participante_correo', 'email'),
			array('participante_nombre, participante_apellido, participante_correo, participante_clave, participante_creacion, participante_modificacion', 'participante_clave_repeat' 'required'),
			array('participante_nombre, participante_apellido', 'length', 'max'=>100),
			array('participante_correo', 'length', 'max'=>150),
			array('participante_clave', 'length', 'max'=>50),
			array('participante_clave_repeat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_participante, participante_nombre, participante_apellido, participante_correo, participante_clave, participante_creacion, participante_modificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compras' => array(self::HAS_MANY, 'Compra', 'compra_idt_participante'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_participante' => 'Idt Participante',
			'participante_nombre' => 'Participante Nombre',
			'participante_apellido' => 'Participante Apellido',
			'participante_correo' => 'Participante Correo',
			'participante_clave' => 'Participante Clave',
			'participante_creacion' => 'Participante Creacion',
			'participante_modificacion' => 'Participante Modificacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_participante',$this->idt_participante,true);
		$criteria->compare('participante_nombre',$this->participante_nombre,true);
		$criteria->compare('participante_apellido',$this->participante_apellido,true);
		$criteria->compare('participante_correo',$this->participante_correo,true);
		$criteria->compare('participante_clave',$this->participante_clave,true);
		$criteria->compare('participante_creacion',$this->participante_creacion,true);
		$criteria->compare('participante_modificacion',$this->participante_modificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
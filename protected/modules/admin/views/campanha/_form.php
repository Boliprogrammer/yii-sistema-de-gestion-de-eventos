<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'campanha-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'campanha_nombre'); ?>
		<?php echo $form->textField($model,'campanha_nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'campanha_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campanha_asunto'); ?>
		<?php echo $form->textField($model,'campanha_asunto',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'campanha_asunto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campanha_mensaje'); ?>
		<?php echo $form->textArea($model,'campanha_mensaje',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'campanha_mensaje'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'campanha_idt_evento'); ?>
		<?php echo $form->textField($model,'campanha_idt_evento',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'campanha_idt_evento'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Modificar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
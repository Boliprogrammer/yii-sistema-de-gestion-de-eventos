<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<!-- <?php echo $form->label($model,'idt_campanha'); ?> -->
		<!-- <?php echo $form->textField($model,'idt_campanha',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'campanha_nombre'); ?>
		<?php echo $form->textField($model,'campanha_nombre',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campanha_asunto'); ?>
		<?php echo $form->textField($model,'campanha_asunto',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campanha_mensaje'); ?>
		<?php echo $form->textArea($model,'campanha_mensaje',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'campanha_idt_evento'); ?> -->
		<!-- <?php echo $form->textField($model,'campanha_idt_evento',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
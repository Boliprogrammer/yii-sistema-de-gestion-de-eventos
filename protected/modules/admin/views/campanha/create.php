<?php
$this->breadcrumbs=array(
	'Campanhas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar campanha', 'url'=>array('index')),
	array('label'=>'Buscar campanha', 'url'=>array('admin')),
);
?>

<h1>Cree su campa&ntilde;a</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Eventos'=>array('evento/index'),
	'Evento actual'=>array('evento/view', 'id'=>Yii::app()->user->getState('idt_evento_actual')),
	'Campanhas',
);

$this->menu=array(
	array('label'=>'Crear campanha', 'url'=>array('create')),
	array('label'=>'Buscar campanha', 'url'=>array('admin')),
);
?>

<h1>Campa&ntilde;as</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

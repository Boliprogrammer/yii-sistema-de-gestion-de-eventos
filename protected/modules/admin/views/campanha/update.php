<?php
$this->breadcrumbs=array(
	'Campanhas'=>array('index'),
	$model->campanha_nombre=>array('view','id'=>$model->idt_campanha),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar Campanhas', 'url'=>array('index')),
	array('label'=>'Crear Campanha', 'url'=>array('create')),
	array('label'=>'Ver Campanha', 'url'=>array('view', 'id'=>$model->idt_campanha)),
	array('label'=>'Buscar Campanha', 'url'=>array('admin')),
);
?>

<h1>Actualizar Campanha: <?php echo $model->campanha_nombre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
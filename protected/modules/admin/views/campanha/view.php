<?php
$this->breadcrumbs=array(
	'Eventos'=>array('evento/index'),
	'Evento actual'=>array('evento/view', 'id'=>Yii::app()->user->getState('idt_evento_actual')),
	'Campanhas'=>array('index'),
	//'Campanha Actual'=>array('campanha/view', 'id'=>Yii::app()->user->getState('idt_campanha_actual')),
	$model->campanha_nombre,
);

$this->menu=array(
	array('label'=>'Listar campanha', 'url'=>array('index')),
	array('label'=>'Crear campanha', 'url'=>array('create')),
	array('label'=>'Modificar campanha', 'url'=>array('update', 'id'=>$model->idt_campanha)),
	array('label'=>'Eliminar campanha', 'url'=>'#', 'linkOptions'=>array(
														'submit'=>array('delete','id'=>$model->idt_campanha),
														'confirm'=>'Esta seguro que desea eliminar a '. $model->campanha_nombre . '?',
														'csrf'=>true,
													),
												),
	array('label'=>'Buscar campanha', 'url'=>array('admin')),
	array('label'=>'Listar invitaciones', 'url'=>array('/admin/invitacion/index')),
	array('label'=>'Enviar invitacion', 'url'=>array('/admin/invitacion/create')),
);
?>

<h1>Campa&ntilde;a: <?php echo $model->campanha_nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idt_campanha',
		'campanha_nombre',
		'campanha_asunto',
		'campanha_mensaje',
		//'campanha_idt_evento',
	),
)); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_nombre'); ?>
		<?php echo $form->textField($model,'contacto_nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'contacto_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_apellido'); ?>
		<?php echo $form->textField($model,'contacto_apellido',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'contacto_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_correo'); ?>
		<?php echo $form->textField($model,'contacto_correo',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'contacto_correo'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'contacto_idt_organizador'); ?>
		<?php echo $form->textField($model,'contacto_idt_organizador',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'contacto_idt_organizador'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Modificar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<!-- <?php echo $form->label($model,'idt_contacto'); ?> -->
		<!-- <?php echo $form->textField($model,'idt_contacto',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto_nombre'); ?>
		<?php echo $form->textField($model,'contacto_nombre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto_apellido'); ?>
		<?php echo $form->textField($model,'contacto_apellido',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contacto_correo'); ?>
		<?php echo $form->textField($model,'contacto_correo',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'contacto_idt_organizador'); ?> -->
		<!-- <?php echo $form->textField($model,'contacto_idt_organizador',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>
	



<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
$this->breadcrumbs=array(
	'Contactos'=>array('index'),
	'Buscar',
);

$this->menu=array(
	array('label'=>'Listar Contacto', 'url'=>array('index')),
	array('label'=>'Agregar Contacto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('contacto-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Buscar contacto</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<?php echo CHtml::link('Busquda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
//Yii::import('application.extensions.FixedCheckboxColumn.FixedCheckboxColumn');
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'contacto-grid',
    'dataProvider'=>$model->search(),
    'selectableRows' => 2,
    'filter'=>$model,
    'columns'=>array(
		//'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		//'contacto_idt_organizador',
        /* array(
            'class'=>'CCheckBoxColumn',
            'checked' => 'false',
        ),*/
		array(
			'class'=>'CButtonColumn',
		),
    ),
)); ?>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contacto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		'contacto_idt_organizador',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); */?>

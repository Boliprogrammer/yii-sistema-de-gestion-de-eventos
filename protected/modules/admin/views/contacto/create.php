<?php
$this->breadcrumbs=array(
	'Contactos'=>array('index'),
	'Agregar',
);

$this->menu=array(
	array('label'=>'Listar Contactos', 'url'=>array('index')),
	array('label'=>'Buscar Contacto', 'url'=>array('admin')),
);
?>

<h1>Agregue un nuevo contacto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
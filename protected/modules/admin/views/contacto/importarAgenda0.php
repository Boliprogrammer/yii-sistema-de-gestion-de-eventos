<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacto-importarAgenda-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_nombre'); ?>
		<?php echo $form->textField($model,'contacto_nombre'); ?>
		<?php echo $form->error($model,'contacto_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_apellido'); ?>
		<?php echo $form->textField($model,'contacto_apellido'); ?>
		<?php echo $form->error($model,'contacto_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_correo'); ?>
		<?php echo $form->textField($model,'contacto_correo'); ?>
		<?php echo $form->error($model,'contacto_correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto_idt_organizador'); ?>
		<?php echo $form->textField($model,'contacto_idt_organizador'); ?>
		<?php echo $form->error($model,'contacto_idt_organizador'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
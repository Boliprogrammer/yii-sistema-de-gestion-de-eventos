<?php
$this->breadcrumbs=array(
	'Contactos'=>array('index'),
	'Importar contactos',
);

$this->menu=array(
	array('label'=>'Listar Contactos', 'url'=>array('index')),
	array('label'=>'Agregar Contacto', 'url'=>array('create')),
);

/* Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('contacto-grid', {
		data: $(this).serialize()
	});
	return false;
});
"); */
?>

<h1>Importar contactos</h1>


<!-- <div class="search-form" style="display:none"> -->
<?php /* $this->renderPartial('_search',array(
	'model'=>$model,
)); */ 
echo CHtml::beginForm(array('invitacion/create'));


?>
<!-- </div> --> <!-- search-form -->

<?php
  $this->widget('ext.selgridview.SelGridView', array(
	'id' => 'contacto-grid',
	'dataProvider' => $model->search(), /*$dataProvider*/
	'selectableRows' => 2,
	'filter' => $model,
	'columns'=>array(
		array(
			/*'name' => '',
			'value' => 'CHtml::checkBox("cid[]",null,array("value"=>$data->id,"id"=>"contacto_correo" ))',
			'type' => 'raw',
			'htmlOptions' => array('width' => 5),*/
			'class' => 'CCheckBoxColumn',
		    'checked' => 'false',
			//'value' => '$data->idt_contacto',
			//'id' => 'contacto_correo',
		),
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		//'contacto_idt_organizador',

	 ),
  ));

?>

<div class="row buttons">
	<?php
		//echo '<method="post">';
		echo CHtml::hiddenField('oculto');//campo en el que guarda los id
		echo CHtml::submitButton('Importar',
			array(
				'onClick'=>"{valoresSeleccionados();}",
				//'submit'=>Yii::app()->controller->createUrl('invitacion/importar'),
				//'submit'=>Yii::app()->controller->createUrl('invitacion/importar'),
			)
		);//boton que va a la funcion js
		
	//echo CHtml::beginForm($this->createUrl('create'), 'get');
	?>
</div>

<script type="text/javascript">
function valoresSeleccionados()
{
	    var seleccionados= $("#contacto-grid").selGridView("getAllSelection");//esta linea guarda los id separados por una ","
		//$.cookie('the_cookie','valor');
		//$cookie=new CHttpCookie('name',$seleccionados);
		//Yii::app()->request->cookies['name']=$cookie;
        //alert(seleccionados);//si quieres ver esos valores
		<?php /*echo Yii::app()->user->setState('correosSeleccionados',$seleccionados); */?>
        $('#oculto').val(seleccionados);//asigno al campo oculto los campos
}
</script>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contacto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		'contacto_idt_organizador',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); */?>

<?php echo CHtml::endForm(); ?>

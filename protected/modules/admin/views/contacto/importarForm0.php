<?php
$this->breadcrumbs=array(
	//'Contactos'=>array('index'),
	//'Manage',
);

$this->menu=array(
	//array('label'=>'List Contacto', 'url'=>array('index')),
	//array('label'=>'Create Contacto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('contacto-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Importar contactos</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
//Yii::import('application.extensions.FixedCheckboxColumn.FixedCheckboxColumn');
/* $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'contacto-grid',
    'dataProvider'=>$model->search(),
    'selectableRows' => 2,
    'filter'=>$model,
    'columns'=>array(
		//'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		'contacto_idt_organizador',
        array(
            'class'=>'CCheckBoxColumn',
            'checked' => 'false',
        ),
    ),
)); */ ?>


<?php
  $this->widget('ext.selgridview.SelGridView', array(
	'id' => 'contacto-grid',
	'dataProvider' => $model->search(), /*$dataProvider*/
	'selectableRows' => 2,
	'filter' => $model,
	'columns'=>array(
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		'contacto_idt_organizador',
		array(
			/*'name' => '',
			'value' => 'CHtml::checkBox("cid[]",null,array("value"=>$data->id,"id"=>"contacto_correo" ))',
			'type' => 'raw',
			'htmlOptions' => array('width' => 5),*/
			'class' => 'CCheckBoxColumn',
		    'checked' => 'false',
			//'value' => '$data->idt_contacto',
			//'id' => 'contacto_correo',
		),
	 ),
  ));
  
	echo CHtml::button('Importar correos',array('onClick'=>"{valoresSeleccionados();}",));//boton que va a la funcion js
	echo CHtml::textField('oculto',' ');//campo en el que guarda los id
	//echo CHtml::beginForm($this->createUrl('create'), 'get');
?>

<div class="row buttons">
	<?php echo CHtml::submitButton('Importar', array(
		'name'=>'demandeVersement',
				
		'onclick'=>"window.close ('/test/cgi-bin/test.pl', 'nom_interne_de_la_fenetre', config='height=500, width=700, toolbar=yes, menubar=yes, scrollbars=yes, resizable=yes, location=yes, directories=yes, status=yes')"
	));?>	
</div>

<div class="row buttons">
	<?php $this->widget('zii.widgets.jui.CJuiButton',
			array(
				'name'=>'importarButton',
				'caption'=>'Importar seleccionados',
				/*'onclick'=> "{valoresSeleccionados();}",*/
				'onclick'=>'js:function(){alert("Yes"); this.blur(); return false;}',
				'buttonType'=>'button',
			)
	); 
	?>
</div>

<script type="text/javascript">
function valoresSeleccionados()
{
	    var seleccionados= $("#contacto-grid").selGridView("getAllSelection");//esta linea guarda los id separados por una ","
        alert(seleccionados);//si quieres ver esos valores
		/*Yii::app()->user->setState('correosSeleccionados',$seleccionados);*/
        $('#oculto').val(seleccionados);//asigno al campo oculto los campos
}
</script>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contacto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		'contacto_idt_organizador',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); */?>

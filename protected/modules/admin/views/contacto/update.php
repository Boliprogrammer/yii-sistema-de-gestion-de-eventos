<?php
$this->breadcrumbs=array(
	'Contactos'=>array('index'),
	$model->contacto_nombre.' '.$model->contacto_apellido=>array('view','id'=>$model->idt_contacto),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar Contactos', 'url'=>array('index')),
	array('label'=>'Agregar Contacto', 'url'=>array('create')),
	array('label'=>'Ver Contacto', 'url'=>array('view', 'id'=>$model->idt_contacto)),
	array('label'=>'Buscar Contacto', 'url'=>array('admin')),
);
?>

<h1>Actualizar contacto: <?php echo $model->contacto_nombre . ' ' . $model->contacto_apellido; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Contactos'=>array('index'),
	$model->contacto_nombre . " " . $model->contacto_apellido,
);

$this->menu=array(
	array('label'=>'Listar Contactos', 'url'=>array('index')),
	array('label'=>'Agregar Contacto', 'url'=>array('create')),
	array('label'=>'Modificar Contacto', 'url'=>array('update', 'id'=>$model->idt_contacto)),
	array('label'=>'Eliminar Contacto', 
		'url'=>'#', 
		'linkOptions'=>array(
			'submit'=>array('delete','id'=>$model->idt_contacto),
			'confirm'=>'Esta usted seguro que quiere eliminar a '. $model->contacto_nombre . ' ' . $model->contacto_apellido . '?',
			'csrf'=>true,
			),
			//'csrf'=>true,
		),
	array('label'=>'Buscar Contacto', 'url'=>array('admin')),
);
?>

<h1>Ver contacto: <?php echo $model->contacto_nombre . " " . $model->contacto_apellido;  ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idt_contacto',
		'contacto_nombre',
		'contacto_apellido',
		'contacto_correo',
		//'contacto_idt_organizador',
	),
)); ?>

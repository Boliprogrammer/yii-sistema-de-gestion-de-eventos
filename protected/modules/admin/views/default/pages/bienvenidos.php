<?php
$this->pageTitle=Yii::app()->name . ' - Bienvenidos';
$this->breadcrumbs=array(
	'Bienvenidos',
);
?>
<h1>Bienvenidos</h1>

<p>Desde ahora en adelante podras crear tus propios eventos, promocinarlos y permitirles a tus participantes pagos en linea.</p>
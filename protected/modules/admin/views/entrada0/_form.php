<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'entrada-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_nombre'); ?>
		<?php echo $form->textField($model,'entrada_nombre',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'entrada_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_precio'); ?>
		<?php echo $form->textField($model,'entrada_precio'); ?>
		<?php echo $form->error($model,'entrada_precio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_cantidadMaxima'); ?>
		<?php echo $form->textField($model,'entrada_cantidadMaxima',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'entrada_cantidadMaxima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_habilitadoDesde'); ?>
		<?php echo $form->textField($model,'entrada_habilitadoDesde'); ?>
		<?php echo $form->error($model,'entrada_habilitadoDesde'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_habilitadoHasta'); ?>
		<?php echo $form->textField($model,'entrada_habilitadoHasta'); ?>
		<?php echo $form->error($model,'entrada_habilitadoHasta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entrada_idt_evento'); ?>
		<?php echo $form->textField($model,'entrada_idt_evento',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'entrada_idt_evento'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
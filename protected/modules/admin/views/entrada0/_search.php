<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idt_entrada'); ?>
		<?php echo $form->textField($model,'idt_entrada',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_nombre'); ?>
		<?php echo $form->textField($model,'entrada_nombre',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_precio'); ?>
		<?php echo $form->textField($model,'entrada_precio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_cantidadMaxima'); ?>
		<?php echo $form->textField($model,'entrada_cantidadMaxima',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_habilitadoDesde'); ?>
		<?php echo $form->textField($model,'entrada_habilitadoDesde'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_habilitadoHasta'); ?>
		<?php echo $form->textField($model,'entrada_habilitadoHasta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada_idt_evento'); ?>
		<?php echo $form->textField($model,'entrada_idt_evento',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
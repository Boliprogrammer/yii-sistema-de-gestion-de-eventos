<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idt_entrada')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idt_entrada), array('view', 'id'=>$data->idt_entrada)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_precio')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_precio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_cantidadMaxima')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_cantidadMaxima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_habilitadoDesde')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_habilitadoDesde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_habilitadoHasta')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_habilitadoHasta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada_idt_evento')); ?>:</b>
	<?php echo CHtml::encode($data->entrada_idt_evento); ?>
	<br />


</div>
<?php
$this->breadcrumbs=array(
	'Entradas'=>array('index'),
	$model->idt_entrada=>array('view','id'=>$model->idt_entrada),
	'Update',
);

$this->menu=array(
	array('label'=>'List Entrada', 'url'=>array('index')),
	array('label'=>'Create Entrada', 'url'=>array('create')),
	array('label'=>'View Entrada', 'url'=>array('view', 'id'=>$model->idt_entrada)),
	array('label'=>'Manage Entrada', 'url'=>array('admin')),
);
?>

<h1>Update Entrada <?php echo $model->idt_entrada; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Entradas'=>array('index'),
	$model->idt_entrada,
);

$this->menu=array(
	array('label'=>'List Entrada', 'url'=>array('index')),
	array('label'=>'Create Entrada', 'url'=>array('create')),
	array('label'=>'Update Entrada', 'url'=>array('update', 'id'=>$model->idt_entrada)),
	array('label'=>'Delete Entrada', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idt_entrada),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Entrada', 'url'=>array('admin')),
);
?>

<h1>View Entrada #<?php echo $model->idt_entrada; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idt_entrada',
		'entrada_nombre',
		'entrada_precio',
		'entrada_cantidadMaxima',
		'entrada_habilitadoDesde',
		'entrada_habilitadoHasta',
		'entrada_idt_evento',
	),
)); ?>

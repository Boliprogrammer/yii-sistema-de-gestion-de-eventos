<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<!-- <?php echo $form->label($model,'idt_evento'); ?> -->
		<!-- <?php echo $form->textField($model,'idt_evento',array('size'=>20,'maxlength'=>20)); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_nombre'); ?>
		<?php echo $form->textField($model,'evento_nombre',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_fecha_inicio'); ?>
		<?php echo $form->textField($model,'evento_fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_fecha_fin'); ?>
		<?php echo $form->textField($model,'evento_fecha_fin'); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'evento_tipo_evento'); ?> -->
		<!-- <?php echo $form->textField($model,'evento_tipo_evento'); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_lugar'); ?>
		<?php echo $form->textField($model,'evento_lugar',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_cupo_maximo'); ?>
		<?php echo $form->textField($model,'evento_cupo_maximo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'evento_categoria'); ?> -->
		<!-- <?php echo $form->textField($model,'evento_categoria',array('size'=>3,'maxlength'=>3)); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'evento_descripcion'); ?>
		<?php echo $form->textArea($model,'evento_descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'evento_estado'); ?> -->
		<!-- <?php echo $form->textField($model,'evento_estado',array('size'=>1,'maxlength'=>1)); ?> -->
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'evento_idt_organizador'); ?> -->
		<!-- <?php echo $form->textField($model,'evento_idt_organizador',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
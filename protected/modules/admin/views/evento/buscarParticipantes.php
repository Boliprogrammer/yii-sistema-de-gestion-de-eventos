<?php
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	'Ver Participantes para este evento',
);

/* $this->menu=array(
	array('label'=>'List Participante', 'url'=>array('index')),
	array('label'=>'Create Participante', 'url'=>array('create')),
); */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('participante-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Ver los Participantes para  este Evento</h1>

<p>
Pueden opcionalmente utilizar un operador de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al comienzo de cada uno de tus valores de busqueda para especificar la comparacion.
</p>

<!-- <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?> -->
<div class="search-form" style="display:none">
<?php $this->renderPartial('_searchParticipantes',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'participante-grid',
	'dataProvider'=>$model->buscarParticipantes(),
	'filter'=>$model,
	'columns'=>array(
		//'idt_participante',
		'participante_nombre',
		'participante_apellido',
		'participante_correo',
		//'participante_clave',
		'participante_creacion',
		/*
		'participante_modificacion',
		*/
		/* array(
			'class'=>'CButtonColumn',
		), */
	),
)); ?>

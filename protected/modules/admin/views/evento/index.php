<?php
$this->breadcrumbs=array(
	'Eventos',
);

$this->menu=array(
	array('label'=>'Crear evento', 'url'=>array('create')),
	array('label'=>'Buscar evento', 'url'=>array('admin')),
);
?>

<h1>Eventos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

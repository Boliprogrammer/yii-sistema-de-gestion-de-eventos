<?php
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	$model->evento_nombre=>array('view','id'=>$model->idt_evento),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Eventos', 'url'=>array('index')),
	array('label'=>'Crear Evento', 'url'=>array('create')),
	array('label'=>'Ver Evento', 'url'=>array('view', 'id'=>$model->idt_evento)),
	array('label'=>'Buscar Evento', 'url'=>array('admin')),
);
?>

<h1>Actualizar Evento: <?php echo $model->evento_nombre; ?></h1>

<?php /* echo $this->renderPartial('_form', array('model'=>$model)); */ ?>
<?php echo $this->renderPartial('_form', array('model'=>$model,
								'entrada'=>$entrada,'validatedEntradas'=>$validatedEntradas)); ?>
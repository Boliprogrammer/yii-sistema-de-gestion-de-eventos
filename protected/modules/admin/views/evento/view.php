<?php
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	$model->evento_nombre,
);

$this->menu=array(
	array('label'=>'Listar eventos', 'url'=>array('index')),
	array('label'=>'Crear evento', 'url'=>array('create')),
	array('label'=>'Modificar evento', 'url'=>array('update', 'id'=>$model->idt_evento)),
	array('label'=>'Eliminar evento', 
		'url'=>'#', 
		'linkOptions'=>array(
			'submit'=>array('delete','id'=>$model->idt_evento),
			'confirm'=>'Esta seguro que quiere eliminar este evento?',
			'csrf'=>true,
			),
		),
	array('label'=>'Buscar Evento', 'url'=>array('admin')),
	array('label'=>'Gestionar Campanhas', 'url'=>array('/admin/campanha/index')),
	array('label'=>'Ver Participantes de este Evento', 'url'=>array('buscarParticipantes')),
);
?>

<h1><?php echo $model->evento_nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idt_evento',
		'evento_nombre',
		'evento_fecha_inicio',
		'evento_fecha_fin',
		//'evento_tipo_evento',
		'evento_lugar',
		'evento_cupo_maximo',
		//'evento_categoria',
		'evento_descripcion',
		//'ventas_totales',
		//'evento_estado',
		//'evento_idt_organizador',
		array(
			'label'=>'VentasTotales',
			'type'=>'raw',
			'value'=>$model->obtenerIngresoXEvento(),
		),
	),
)); ?>

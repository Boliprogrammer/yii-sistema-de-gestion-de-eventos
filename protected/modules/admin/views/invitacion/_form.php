<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invitacion-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php 
		//$model->invitacion_correos = Yii::app()->request->cookies->contains('the_cookie') ? Yii::app()->request->cookies['the_cookie']->value : '';
		/* $model->invitacion_correos=Yii::app()->user->getState('correosSeleccionados'); */
/*Yii::app()->user->getState('correosSeleccionados'); */?>
		<?php echo $form->labelEx($model,'invitacion_correos'); ?>
		<?php echo $form->textArea($model,'invitacion_correos',array('rows'=>7, 'cols'=>70)); ?>
		<?php echo $form->error($model,'invitacion_correos'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'invitacion_cantidad'); ?>
		<?php echo $form->textField($model,'invitacion_cantidad'); ?>
		<?php echo $form->error($model,'invitacion_cantidad'); ?>
	</div> -->

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'invitacion_resultado'); ?>
		<?php echo $form->textField($model,'invitacion_resultado'); ?>
		<?php echo $form->error($model,'invitacion_resultado'); ?>
	</div> -->

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'invitacion_idt_campanha'); ?>
		<?php echo $form->textField($model,'invitacion_idt_campanha',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'invitacion_idt_campanha'); ?>
	</div> -->
	<h4><i><font size=2>Correos separados por comas: ejemplo1@correo.com, ejemplo2@correo.com, ejemplo3@correo.com </font><i></h4>
	
	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
			<div>
				<?php $this->widget('CCaptcha'); ?>
				<?php echo $form->textField($model,'verifyCode'); ?>
			</div>
			<div class="hint">Por favor ingresa las letras como se muestran.
			<br/>Letras no son sensibles.</div>
			<?php echo $form->error($model,'verifyCode');  ?>
		</div>
    <?php endif; ?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Enviar' : 'Modificar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
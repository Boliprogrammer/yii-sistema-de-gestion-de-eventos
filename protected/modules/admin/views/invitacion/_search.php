<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<!-- <?php echo $form->label($model,'idt_invitacion'); ?> -->
		<!-- <?php echo $form->textField($model,'idt_invitacion',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row">
		<?php echo $form->label($model,'invitacion_correos'); ?>
		<?php echo $form->textArea($model,'invitacion_correos',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'invitacion_cantidad'); ?> -->
		<!-- <?php echo $form->textField($model,'invitacion_cantidad'); ?> -->
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'invitacion_resultado'); ?> -->
		<!-- <?php echo $form->textField($model,'invitacion_resultado'); ?> -->
	</div>

	<div class="row">
		<!-- <?php echo $form->label($model,'invitacion_idt_campanha'); ?> -->
		<!-- <?php echo $form->textField($model,'invitacion_idt_campanha',array('size'=>10,'maxlength'=>10)); ?> -->
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
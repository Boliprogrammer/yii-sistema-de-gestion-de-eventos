<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idt_invitacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idt_invitacion), array('view', 'id'=>$data->idt_invitacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invitacion_correos')); ?>:</b>
	<?php echo CHtml::encode($data->invitacion_correos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invitacion_cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->invitacion_cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invitacion_resultado')); ?>:</b>
	<?php echo CHtml::encode($data->invitacion_resultado); ?>
	<br />

	<!-- <b><?php echo CHtml::encode($data->getAttributeLabel('invitacion_idt_campanha')); ?>:</b> -->
	<!-- <?php echo CHtml::encode($data->invitacion_idt_campanha); ?> -->
	<!-- <br /> -->


</div>
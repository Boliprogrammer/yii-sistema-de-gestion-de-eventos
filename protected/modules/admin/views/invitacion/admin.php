<?php
$this->breadcrumbs=array(
	'Invitacion'=>array('index'),
	'Buscar',
);

$this->menu=array(
	array('label'=>'Listar Invitaciones', 'url'=>array('index')),
	array('label'=>'Enviar Invitacion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('invitacion-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Buscar Invitaciones</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'invitacion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idt_invitacion',
		'invitacion_correos',
		//'invitacion_cantidad',
		//'invitacion_resultado',
		//'invitacion_idt_campanha',
		/* array(
			'class'=>'CButtonColumn',
		), */
	),
)); ?>

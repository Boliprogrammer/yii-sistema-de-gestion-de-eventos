<?php
$this->breadcrumbs=array(
	'Eventos'=>array('evento/index'),
	'Evento actual'=>array('evento/view', 'id'=>Yii::app()->user->getState('idt_evento_actual')),
	'Campanhas'=>array('index'),
	'Campanha Actual'=>array('campanha/view', 'id'=>Yii::app()->user->getState('idt_campanha_actual')),
	//$model->campanha_nombre,
	//'Invitaciones'=>array('index'),
	'Invitar',
);

$this->menu=array(
	array('label'=>'Listar invitaciones', 'url'=>array('index')),
	array('label'=>'Buscar invitacion', 'url'=>array('admin')),
	//array('label'=>'Agregar link del evento', 'url'=>array('admin')),
	array('label'=>'Importar correos desde la agenda de contactos', 'url'=>array('/admin/contacto/importar') /*, 'linkOptions'=>array('target'=>'_blank')*/),
);
?>

<h1>Enviar invitaciones</h1>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>


<?php
$this->breadcrumbs=array(
	'Eventos'=>array('evento/index'),
	'Evento actual'=>array('evento/view', 'id'=>Yii::app()->user->getState('idt_evento_actual')),
	'Campanhas'=>array('index'),
	'Campanha Actual'=>array('campanha/view', 'id'=>Yii::app()->user->getState('idt_campanha_actual')),
	//$model->campanha_nombre,
	//'Invitaciones'=>array('index'),
	'Invitar',
	//'Invitacion',
);

$this->menu=array(
	array('label'=>'Enviar invitacion', 'url'=>array('create')),
	array('label'=>'Buscar invitacion', 'url'=>array('admin')),
);
?>

<h1>Invitaciones</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

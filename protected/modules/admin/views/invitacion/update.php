<?php
$this->breadcrumbs=array(
	'Invitacions'=>array('index'),
	$model->idt_invitacion=>array('view','id'=>$model->idt_invitacion),
	'Update',
);

$this->menu=array(
	array('label'=>'List Invitacion', 'url'=>array('index')),
	array('label'=>'Create Invitacion', 'url'=>array('create')),
	array('label'=>'View Invitacion', 'url'=>array('view', 'id'=>$model->idt_invitacion)),
	array('label'=>'Manage Invitacion', 'url'=>array('admin')),
);
?>

<h1>Update Invitacion <?php echo $model->idt_invitacion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
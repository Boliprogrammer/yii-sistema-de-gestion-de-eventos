<?php
$this->breadcrumbs=array(
	'Invitacion'=>array('index'),
	$model->idt_invitacion,
);

$this->menu=array(
	array('label'=>'Listar invitaciones', 'url'=>array('index')),
	array('label'=>'Enviar invitacion', 'url'=>array('create')),
	//array('label'=>'OJO PRUEBA Actualizar invitacion', 'url'=>array('update', 'id'=>$model->idt_invitacion)),
	//array('label'=>'Eliminar invitacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idt_invitacion),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Buscar invitacion', 'url'=>array('admin')),
);
?>

<h1>Ver invitacion<?php /*echo $model->idt_invitacion;*/ ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idt_invitacion',
		'invitacion_correos',
		//'invitacion_cantidad',
		//'invitacion_resultado',
		//'invitacion_idt_campanha',
	),
)); ?>

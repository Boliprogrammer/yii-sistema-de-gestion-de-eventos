<?php

class EventosModule extends CWebModule
{
	//public $captcha = array('registration'=>true);

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'eventos.models.*',
			'eventos.components.*',
			'application.modules.admin.models.Evento',
			//'application.modules.admin.controllers.EventoController',
			//'application.modules.admin.views.Evento.*',
			'application.models.LoginForm',
			
			//'application.controllers.SiteController',
			'eventos.modules.*',
			//'application.modules.admin.controllers.EventoController',
		));
		
		//Yii::app()->setComponents('user', Yii::app()->participanteUser);
		$this->setComponents(array(
            'errorHandler' => array(
                'errorAction' => '/eventos/loginout/error'),
            'user' => array(
            'class' => 'CWebUser',
			//'class' => 'CCaptchaAction',
			//'class' => Yii::app()->participanteUser,				
            'loginUrl' => Yii::app()->createUrl('eventos/loginout/login'),
            )
        ));
		Yii::app()->user->setStateKeyPrefix('_eventos');
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			Yii::app()->errorHandler->errorAction='eventos/loginout/error';

			$controller->layout = 'main';
			$controller->layout = 'column1';
			$controller->layout = 'column2';
			// this method is called before any module controller action is performed
			// you may place customized code here
			// this method is called before any module controller action is performed
            // you may place customized code here
            $route = $controller->id . '/' . $action->id;
           // echo $route;
            $publicPages = array(
                'participante/captcha',
				'loginout/captcha',
				'loginout/login',
				'loginout/error',
				'loginout/logout',
				'default/login',
                'default/error',
				'default/logout',
				'participante/create',
				'shop/entrada/index',
				'shop/entrada/view',
				'evento/view',
			);
			
            if (Yii::app()->user->isGuest && !in_array($route, $publicPages)){            
                //Yii::app()->getModule('eventos')->user->loginRequired();                
				Yii::app()->getModule('eventos')->user->loginRequired();                
			}	
            else
                return true;
		}
		else
			return false;
	}
}

<?php

class LoginoutController extends Controller
{
	public function filters()
	{
		return array
		(
			'https +login', // Force https, but only on login page
		);
	} 	

	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
			 'actions' => array('captcha'), 
			 'users' => array('*'),
			 //'expression'=>'Yii::app()->controller->module->allowCaptcha',
			 ),
		);
	}
	
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				//'minLength'=>1,
				//'maxLength'=>10,
				
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
				//$this->redirect(Yii::app()->controller->admin->returnUrl);
				//$this->redirect(array('evento/index'));
				$this->redirect(array('/eventos/loginout/index'));
				//$this->redirect(array('//eventos/shop/entrada/index'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	// Login basado en Modulo o Module Based Login
	public function actionLogout($token) 
	{
		if ($token !== Yii::app()->getRequest()->getCsrfToken())
			throw new CHttpException(40066, Yii::t('app', 'Peticion invalida. Por favor no repita esta peticion otra vez.'));
		else
		{
			Yii::app()->getModule('eventos')->user->logout();
			//$this->redirect(Yii::app()->getModule('eventos')->user->loginUrl);
			$this->redirect(array('/eventos/evento/index'));
		}
    }
	
}
<?php

/**
 * This is the model class for table "t_evento".
 *
 * The followings are the available columns in table 't_evento':
 * @property string $idt_evento
 * @property string $evento_nombre
 * @property string $evento_fecha_inicio
 * @property string $evento_fecha_fin
 * @property integer $evento_tipo_evento
 * @property string $evento_lugar
 * @property string $evento_cupo_maximo
 * @property string $evento_categoria
 * @property string $evento_descripcion
 * @property string $evento_estado
 * @property string $evento_idt_organizador
 */
class Evento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Evento the static model class
	 */
	 
	const TYPE_EVENTO_PRIVADO=0;
	const TYPE_EVENTO_PUBLICO=1;
	
	const CATEGORY_ARQUITECTURA_DISE�O_URBANISMO = 0;
	const CATEGORY_CIENCIAS_EMPRESARIALES = 1;
	const CATEGORY_CIENCIAS_JURIDICAS_Y_SOCIALES = 2;
	const CATEGORY_HUMANIDADES_Y_COMUNICACION = 3;
	const CATEGORY_INGENIERIA = 4;
	
	const ESTADO_PAUSADO = 0;
	const ESTADO_HABILITADO = 1;
		
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_evento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('evento_nombre, evento_fecha_inicio, evento_fecha_fin, evento_cupo_maximo, evento_idt_organizador', 'required'),
			array('evento_tipo_evento', 'numerical', 'integerOnly'=>true),
			array('evento_nombre, evento_lugar', 'length', 'max'=>150),
			array('evento_cupo_maximo, evento_idt_organizador', 'length', 'max'=>10),
			array('evento_categoria', 'length', 'max'=>3),
			array('evento_estado', 'length', 'max'=>1),
			array('evento_fecha_inicio, evento_fecha_fin, evento_descripcion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_evento, evento_nombre, evento_fecha_inicio, evento_fecha_fin, evento_tipo_evento, evento_lugar, evento_cupo_maximo, evento_categoria, evento_descripcion, evento_estado, evento_idt_organizador', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'eventoIdtOrganizador' => array(self::BELONGS_TO, 'Organizador', 'evento_idt_organizador'),
			'campanha' => array(self::HAS_MANY, 'Campanha', 'campanha_idt_evento'),
			'entrada' => array(self::HAS_MANY, 'Entrada', 'entrada_idt_evento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_evento' => 'Idt Evento',
			'evento_nombre' => 'Nombre del evento',
			'evento_fecha_inicio' => 'Fecha de inicio',
			'evento_fecha_fin' => 'Fecha de finalizacion',
			'evento_tipo_evento' => 'Tipo de evento',
			'evento_lugar' => 'Lugar',
			'evento_cupo_maximo' => 'Cupo maximo',
			'evento_categoria' => 'Categoria',
			'evento_descripcion' => 'Descripcion',
			'evento_estado' => 'Estado',
			'evento_idt_organizador' => 'Evento Idt Organizador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_evento',$this->idt_evento,true);
		$criteria->compare('evento_nombre',$this->evento_nombre,true);
		$criteria->compare('evento_fecha_inicio',$this->evento_fecha_inicio,true);
		$criteria->compare('evento_fecha_fin',$this->evento_fecha_fin,true);
		$criteria->compare('evento_tipo_evento',$this->evento_tipo_evento);
		$criteria->compare('evento_lugar',$this->evento_lugar,true);
		$criteria->compare('evento_cupo_maximo',$this->evento_cupo_maximo,true);
		$criteria->compare('evento_categoria',$this->evento_categoria,true);
		$criteria->compare('evento_descripcion',$this->evento_descripcion,true);
		$criteria->compare('evento_estado',$this->evento_estado,true);
		$criteria->compare('evento_idt_organizador',$this->evento_idt_organizador,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getTypeEventOptions()
	{
		return array(
			self::TYPE_EVENTO_PRIVADO=>'Privado',
			self::TYPE_EVENTO_PUBLICO=>'Publico',
		);
	}
	
	public function getCategoryOptions()
	{
		return array(
			self::CATEGORY_ARQUITECTURA_DISE�O_URBANISMO=>'Arquitectura, Diseno y Urbanismo',
			self::CATEGORY_CIENCIAS_EMPRESARIALES=>'Ciencias empresariales', 
			self::CATEGORY_CIENCIAS_JURIDICAS_Y_SOCIALES=>'Ciencias juridicas y sociales', 
			self::CATEGORY_HUMANIDADES_Y_COMUNICACION=>'Humanidades y comunicacion', 
			self::CATEGORY_INGENIERIA=>'Ingenieria', 
		);
	}
	
	public function getStateOptions()
	{
		return array(
			self::ESTADO_PAUSADO=>'Pausado',
			self::ESTADO_HABILITADO=>'Habilitado',
		);
	}
}
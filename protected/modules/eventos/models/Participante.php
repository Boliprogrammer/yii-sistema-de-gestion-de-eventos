<?php

/**
 * This is the model class for table "t_participante".
 *
 * The followings are the available columns in table 't_participante':
 * @property string $idt_participante
 * @property string $participante_nombre
 * @property string $participante_apellido
 * @property string $participante_correo
 * @property string $participante_clave
 * @property string $participante_creacion
 * @property string $participante_modificacion
 *
 * The followings are the available model relations:
 * @property Compra[] $compras
 */
class Participante extends ParticipanteActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Participante the static model class
	 */
	public $participante_clave_repeat;
	public $verifyCode;
	
	protected function afterValidate()
	{
		parent::afterValidate();
		$this->participante_clave = $this->encrypt($this->participante_clave);
	}
		
	public function encrypt($value)
	{
		return md5($value);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_participante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('participante_clave', 'compare'),
			array('participante_correo', 'unique'),
			array('participante_correo', 'email'),
			array('participante_nombre, participante_apellido, participante_correo, participante_clave, participante_creacion, participante_modificacion,participante_clave_repeat', 'required'),
			array('participante_nombre, participante_apellido', 'length', 'max'=>100),
			array('participante_correo', 'length', 'max'=>150),
			array('participante_clave', 'length', 'max'=>50),
			array('participante_clave_repeat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_participante, participante_nombre, participante_apellido, participante_correo, participante_clave, participante_creacion, participante_modificacion', 'safe', 'on'=>'search'),
			array('participante_nombre, participante_apellido, participante_correo, participante_clave, participante_clave_repeat', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('participante_clave','ext.validators.EPasswordStrength','min'=>8,'message'=>Yii::t('category','Clave muy debil; al menos 8 caracteres que contengan mayusuculas, minusculas y al menos un numero')),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(),'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'compras' => array(self::HAS_MANY, 'Compra', 'compra_idt_participante'),
			'compras' => array(self::HAS_MANY, 'Order', 'customer_id'),
			'ShoppingCarts' => array(self::HAS_MANY, 'ShoppingCart', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_participante' => 'Idt Participante',
			'participante_nombre' => 'Nombre',
			'participante_apellido' => 'Apellido',
			'participante_correo' => 'Correo',
			'participante_clave' => 'Clave',
			'participante_clave_repeat' => 'Repetir clave',
			'participante_creacion' => 'Participante Creacion',
			'participante_modificacion' => 'Participante Modificacion',
			'verifyCode'=>'Codigo de verificacion'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_participante',$this->idt_participante,true);
		$criteria->compare('participante_nombre',$this->participante_nombre,true);
		$criteria->compare('participante_apellido',$this->participante_apellido,true);
		$criteria->compare('participante_correo',$this->participante_correo,true);
		$criteria->compare('participante_clave',$this->participante_clave,true);
		$criteria->compare('participante_creacion',$this->participante_creacion,true);
		$criteria->compare('participante_modificacion',$this->participante_modificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function buscarParticipantes()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		//$order = new Order;

		$criteria=new CDbCriteria;

		$criteria->compare('idt_participante',$this->idt_participante,true);
		$criteria->compare('participante_nombre',$this->participante_nombre,true);
		$criteria->compare('participante_apellido',$this->participante_apellido,true);
		$criteria->compare('participante_correo',$this->participante_correo,true);
		$criteria->compare('participante_clave',$this->participante_clave,true);
		$criteria->compare('participante_creacion',$this->participante_creacion,true);
		$criteria->compare('participante_modificacion',$this->participante_modificacion,true);
		
		/*$criteria->with = array('compras'=>array(
			'condition'=>'order_id=1'),);*/
		
		/*$sql = 'select * from t_participante, t_shop_order, t_shop_order_position, t_entrada, t_evento where t_evento.idt_evento=t_entrada.entrada_idt_evento and t_shop_order_position.product_id=t_entrada.idt_entrada and t_shop_order_position.order_id=t_shop_order.order_id and t_participante.idt_participante=t_shop_order.customer_id and idt_evento= 43';
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$results = $command->queryAll(); */
		
		//$criteria->condition='t_evento.idt_evento=t_entrada.entrada_idt_evento and t_shop_order_position.product_id=t_entrada.idt_entrada and t_shop_order_position.order_id=t_shop_order.order_id and t_participante.idt_participante=t_shop_order.customer_id and idt_evento= 43';
		//$criteria->condition='order_id=1';
		$criteriaCondition1 = 'idt_evento='.Yii::app()->user->getState('idt_evento_actual');
		$criteria->mergeWith(array(
				'with'=>array(
					'compras'=>array(//puedo acceder a la tabla Order
						//'condition'=>'order_id=63',
						'with'=>array(
							'entradas'=>array(//puedo acceder a la tabla OrderPosition
								//'condition'=>'id=87',
								'with'=>array(
									'entrada'=>array(//tabla Entrada
										//'condition'=>'idt_entrada=26',
										'with'=>array(
											'entradaEvento'=>array(//evento
												//'condition'=>'entrada_idt_evento=48',
												//'condition'=>$criteriaCondition1,
												'with'=>array(
													'eventoEntrada'=>array(
														'condition'=>$criteriaCondition1,
														//'with'=>array('condition'=>$criteria),
													),
												),
											),
										),
									),
								),
							),
						),
					),
				),
				'distinct'=>false,
				'together'=>TRUE,
			)
		);
		
		return new CActiveDataProvider($this, array(
			'pagination' => array(
					'pageSize' => 1000,
			),
			'criteria'=>$criteria,
			/*'criteria'=>array(
				//'with'=>$criteria,
				'with'=>array(
					'compras'=>array(//puedo acceder a la tabla Order
						//'condition'=>'order_id=63',
						'with'=>array(
							'entradas'=>array(//puedo acceder a la tabla OrderPosition
								//'condition'=>'id=87',
								'with'=>array(
									'entrada'=>array(//tabla Entrada
										//'condition'=>'idt_entrada=26',
										'with'=>array(
											'entradaEvento'=>array(//evento
												//'condition'=>'entrada_idt_evento=48',
												//'condition'=>$criteriaCondition1,
												'with'=>array(
													'eventoEntrada'=>array(
														'condition'=>$criteriaCondition1,
														//'with'=>array('condition'=>$criteria),
													),
												),
											),
										),
									),
								),
							),
						),
					),
				),
				'distinct'=>false,
				'together'=>TRUE,
			),*/
			//'criteria'=>array('condition'=>$criteriaCondition1,),
		));
	}
}
<?php

class ShopModule extends CWebModule
{
	public $version = '0.7';

	// Is the Shop in debug Mode?
	public $debug = false;

  // Whether the installer should install some demo data
	public $installDemoData = true;

	// Enable this to use the shop module together with the yii user
	// management module
	public $useWithYum = true;

	// Names of the tables
	public $categoryTable = 't_shop_category';
	public $productsTable = 't_entrada';
	public $orderTable = 't_shop_order';
	public $orderPositionTable = 't_shop_order_position';
	public $customerTable = 't_participante';
	public $addressTable = 't_shop_address';
	public $imageTable = 't_shop_image';
	public $shippingMethodTable = 't_shop_shipping_method';
	public $paymentMethodTable = 't_shop_payment_method';
	public $taxTable = 't_shop_tax';
	public $productSpecificationTable = 't_shop_product_specification';
	public $productVariationTable = 't_shop_product_variation';
	public $currencySymbol = '$';

	public $logoPath = 'logo.jpg';
	public $slipView = '/order/slip';
	public $invoiceView = '/order/invoice';
	public $footerView = '/order/footer';

	public $dateFormat = 'd/m/Y';
	
	public $imageWidthThumb = 100;
	public $imageWidth = 200;

	public $notifyAdminEmail = null;

	public $termsView = '/order/terms';
	public $successAction = array('//eventos/shop/order/success');
	public $failureAction = array('//eventos/shop/order/failure');

	public $loginUrl = array('/site/login');

	// Where the uploaded product images are stored:
	public $productImagesFolder = 'productimages'; // Approot/...

	//public $layout = 'application.modules.shop.views.layouts.shop';
	public $layout = 'application.modules.eventos.modules.shop.views.layouts.shop';

	public function init()
	{
		$this->setImport(array(
			'shop.models.*',
			'shop.components.*',
			'application.modules.admin.models.*'
		));
	}

	public function beforeControllerAction($controller, $action)
	{
			
		if(parent::beforeControllerAction($controller, $action))
		{
			$controller->layout = 'shop';
			$controller->layout = 'main';
			$controller->layout = 'column1';
			$controller->layout = 'column2';
			
			return true;
		}
		else
			return false;
	}
}

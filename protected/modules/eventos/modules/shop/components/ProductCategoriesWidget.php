<?php
Yii::import('zii.widgets.CPortlet');

class ProductCategoriesWidget extends CPortlet {

	public function init() {
		$this->title = Shop::t('Categorias de Productos');
		return parent::init();
	}

	public function run() {
		$this->render('application.modules.eventos.modules.shop.views.category.index'/*'application.modules.shop.views.category.index'*/);
		return parent::run();
	}
}
?>

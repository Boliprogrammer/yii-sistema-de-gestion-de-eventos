<?php

class EntradaController extends Controller
{
	public $_model;


	public function filters()
	{
		return array(
			'accessControl',
		);
	}	

	public function accessRules() {
		return array(
			array('allow',
				'actions'=>array('view', 'index'),
				'users' => array('*'),
			),
			/* array('allow',
				'actions'=>array('view','index'),
				'users' => array('@'),
			), */
			array('deny',  // deny all other users
					'users'=>array('*'),
			),
		);
	}

	public function beforeAction($action) {
		$this->layout = Shop::module()->layout;
		return parent::beforeAction($action);
	}

	public function actionView()
	{
		if (Yii::app()->user->getState('idt_evento_actual')===null)
			throw new CHttpException(4002,'Peticion invalida. Seleccione un evento.');
		else
		{
			$model=$this->loadModel();
			if (Yii::app()->user->getState('idt_evento_actual')===$model->entrada_idt_evento)
			{
				$this->render('view',array(
					'model'=>$model,
				));
			}
			else
				throw new CHttpException(4001,'Peticion invalida. Por favor no repita esta peticion otra vez.');
		}
	}

	public function actionCreate()
	{
		$model=new Products;

		 $this->performAjaxValidation($model);

		if(isset($_POST['Products']))
		{
			$model->attributes=$_POST['Products'];
			if(isset($_POST['Specifications']))
				$model->setSpecifications($_POST['Specifications']);


			if($model->save())
				$this->redirect(array('shop/admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	// no se puede crear una entrada solamente sin crear un evento.
	public function actionUpdate($id, $return = null)
	{
		$model=$this->loadModel();

		$this->performAjaxValidation($model);

		if(isset($_POST['Products']))
		{
			$model->attributes=$_POST['Products'];
			if(isset($_POST['Specifications']))
				$model->setSpecifications($_POST['Specifications']);
			if(isset($_POST['Variations']))
				$model->setVariations($_POST['Variations']);

			if($model->save())
				if($return == 'product')
					$this->redirect(array('products/update', 'id' => $model->product_id));
				else
					$this->redirect(array('products/admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	 
	// si se quiere eliminar una entrada se debe hacerlo desde la controladora de eventos, a no ser que sea un administrador.
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/************************************
		$criteriaCondition1 = 'entrada_idt_evento='.Yii::app()->user->getState('idt_evento_actual');
		$dataProvider=new CActiveDataProvider('Evento', array(
			'criteria'=>array('condition'=>$criteriaCondition1,),
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		
		
		$criteriaCondition1 = 'entrada_idt_evento='.Yii::app()->user->getState('idt_evento_actual');

		************************************/
		
		if (Yii::app()->user->getState('idt_evento_actual')===null)
			throw new CHttpException(40015,'Peticion invalida. Seleccione un evento.');
		else
		{
			$criteriaCondition1 = 'entrada_idt_evento='.Yii::app()->user->getState('idt_evento_actual');
			$dataProvider = new CActiveDataProvider('Entrada', array(
				'criteria'=>array('condition'=>$criteriaCondition1,),
			));

			$this->render('index',array(
				'dataProvider'=>$dataProvider,
			));
		}
	}

	/**
	 * Manages all models.
	 */
	 
	// la misma analog�a que para "actionDelete". 
	public function actionAdmin()
	{
		$model=new Entrada('search');
		if(isset($_GET['Products']))
			$model->attributes=$_GET['Products'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				//$this->_model=Products::model()->findbyPk($_GET['id']);
				$this->_model=Entrada::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='products-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class OrderController extends Controller
{
	public $_model;

	public function filters()
	{
		return array(
			'accessControl',
			'https + create',
		);
	}	

	public function accessRules() {
		return array(
				/* array('allow',
					'actions'=>array('captcha','view', 'create', 'confirm', 'success', 'failure'),
					'users' => array('*'),
					), */
				array('allow',
					'actions'=>array('create', 'confirm', 'success', 'failure', /*'confirmar', 'admin','delete', 'view', 'slip', 'invoice'*/),
					'users' => array('@'),
					),
				array('deny',  // deny all other users
						'users'=>array('*'),
						),
				);
	}
	
	/* public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	} */

	public function actionSlip($id) {
		if($model = $this->loadModel($id))
			$this->render(Shop::module()->slipView, array('model' => $model));
	}

	public function actionInvoice($id) {
		if($model = $this->loadModel($id))
			$this->render(Shop::module()->invoiceView, array('model' => $model));
	}



	public function beforeAction($action) {
		$this->layout = Shop::module()->layout;
		return parent::beforeAction($action);
	}


	public function actionView()
	{
		$this->render('view',array(
					'model'=>$this->loadModel(),
					));
	}

	/** Creation of a new Order 
	 * Before we create a new order, we need to gather Customer information.
	 * If the user is logged in, we check if we already have customer information.
	 * If so, we go directly to the Order confirmation page with the data passed
	 * over. Otherwise we need the user to enter his data, and depending on
	 * whether he is logged in into the system it is saved with his user 
	 * account or once just for this order.	
	 */
	public function actionCreate(
		//'echo'.Yii::app()->request->csrfTokenName;
		//$valor = Yii::app()->request->getCsrfToken();
		//Yii::app()->request->csrfTokenName;
		
			//$customer = null,
			$participante = null,
			$payment_method = null,
			$shipping_method = null) 
	{
		//echo CHtml::beginForm();


		if(isset($_POST['ShippingMethod'])) 
			Yii::app()->user->setState('shipping_method', $_POST['ShippingMethod']);

		if(isset($_POST['PaymentMethod'])) 
			Yii::app()->user->setState('payment_method', $_POST['PaymentMethod']);


		if(isset($_POST['DeliveryAddress']) && $_POST['DeliveryAddress'] === true) {
			if(Address::isEmpty($_POST['DeliveryAddress'])) {
				Shop::setFlash(Shop::t('Delivery address is not complete! Please fill in all fields to set the Delivery address'));
			} else {
				$deliveryAddress = new DeliveryAddress;
				$deliveryAddress->attributes = $_POST['DeliveryAddress'];
				if($deliveryAddress->save()) {
					$model = Shop::getCustomer();

					if(isset($_POST['toggle_delivery']))
						$model->delivery_address_id = $deliveryAddress->id;
					else
						$model->delivery_address_id = 0;
					$model->save(false, array('delivery_address_id'));
				}
			}
		}

		if(isset($_POST['BillingAddress']) && $_POST['BillingAddress'] === true) {
			if(Address::isEmpty($_POST['BillingAddress'])) {
				Shop::setFlash(Shop::t('Billing address is not complete! Please fill in all fields to set the Billing address'));
			} else {
				$BillingAddress = new BillingAddress;
				$BillingAddress->attributes = $_POST['BillingAddress'];
				if($BillingAddress->save()) {
					$model = Shop::getCustomer();
					if(isset($_POST['toggle_billing']))
						$model->billing_address_id = $BillingAddress->id;
					else
						$model->billing_address_id = 0;
					$model->save(false, array('billing_address_id'));
				}
			}
		}

		if(!$participante)
			$participante = Yii::app()->user->getState('customer_id');
		if(!Yii::app()->user->isGuest && !$participante)
			$participante = Participante::model()->find('idt_participante = :idt_participante ', array(
				':idt_participante' => Yii::app()->user->id));
		if(!$payment_method)
			$payment_method = Yii::app()->user->getState('payment_method');
		if(!$shipping_method)
			$shipping_method = Yii::app()->user->getState('shipping_method');

		if(!$participante) {
			$this->render('/customer/create', array(
						'action' => array('//eventos/shop/customer/create')));
			Yii::app()->end();
		}
		if(!$shipping_method) {
			$this->render('/shippingMethod/choose', array(
						'participante' => Shop::getCustomer()));
			Yii::app()->end();
		}
		if(!$payment_method) {
			$this->render('/paymentMethod/choose', array(
						'customer' => Shop::getCustomer()));
			Yii::app()->end();
		}


		if($participante && $payment_method && $shipping_method)   {
			if(is_numeric($participante))
				$participante = Participante::model()->findByPk($participante);
			if(is_numeric($shipping_method))
				$shipping_method = ShippingMethod::model()->findByPk($shipping_method);
			if(is_numeric($payment_method))
				$payment_method = PaymentMethod::model()->findByPk($payment_method);

			$this->render('/order/create', array(
						'customer' => $participante,
						'shippingMethod' => $shipping_method,
						'paymentMethod' => $payment_method,
						));
		}
		//echo CHtml::endForm();
	}
	
	// SOLO PARA PRUEBAS
	
	public function actionConfirmar()
	{
		//Yii::app()->user->setState('cart', array());
		$this->render('confirmar');
		/*
		$order = new Order();
		$participante = Shop::getCustomer();
		$cart = Shop::getCartContent();
		$cantidadTotal = 0;
		$order->customer_id = $participante->idt_participante;

		foreach($cart as $position => $entrada) 
		{
			$cantidadTotal = $cantidadTotal + $entrada['amount'];
		}

		$order->ordering_date = new CDbExpression('NOW()');
		$order->payment_method = Yii::app()->user->getState('payment_method');
		$order->shipping_method = Yii::app()->user->getState('shipping_method');
		$order->comment = Yii::app()->user->getState('order_comment');
		$order->montoTotal = Shop::getJustPriceTotal();
		$order->cantidadTotal = $cantidadTotal;


		if($order->save()) {
			foreach($cart as $position => $entrada) {
				$position = new OrderPosition;
				$position->order_id = $order->order_id;
				$position->product_id = $entrada['idt_entrada'];
				$position->amount = $entrada['amount'];
				$position->specifications = @json_encode($product['Variations']);
				$position->save();
				Yii::app()->user->setState('cart', array());
				Yii::app()->user->setState('shipping_method', null);
				Yii::app()->user->setState('payment_method', null);
				Yii::app()->user->setState('order_comment', null);
			}
			Shop::mailNotification($order);
			
			$this->redirect(Shop::module()->successAction); //original
		} else 
			$this->redirect(Shop::module()->failureAction);
		*/
	}
	
	
	//EL VERDADERO
	public function actionConfirm() 
	{
		if (Yii::app()->request->IsValidateCsrfToken())
		{
		
			//echo CHtml::beginForm();
			//$valor = @$_POST['Order']['YII_CSRF_TOKEN'];
			//if ($_POST['Order']['YII_CSRF_TOKEN'] === null)
			//throw new CHttpException(4015,'Oh! CSRF encontrado.');
			//$valor = $_POST['YII_CSRF_TOKEN'];
			//Yii::app()->request->getCsrfToken();
			//Yii::app()->user->setState('csrf', @$_POST['Order']['lr_acc']);
			//$valor = @$_POST['Order']['YII_CSRF_TOKEN'];
			//Shop::setFlash($valor);
			//echo '$<a href="http://">Algo</a>';
		
		/*if(@$_POST['Order']['YII_CSRF_TOKEN'] === Yii::app()->request->getCsrfToken())
		{*/
				// only validate POST requests
	
			Yii::app()->user->setState('order_comment', @$_POST['Order']['Comment']);
			//if(isset($_POST['accept_terms']) && $_POST['accept_terms'] == 1) {
			if(!isset($_POST['accept_terms'])) 
			{
				$order = new Order();
				$participante = Shop::getCustomer();
				$cart = Shop::getCartContent();
				$cantidadTotal = 0;
				$order->customer_id = $participante->idt_participante;

				/* $address = new DeliveryAddress();
				if($participante->deliveryAddress)
					$address->attributes = $participante->deliveryAddress->attributes;
				else
					$address->attributes = $participante->address->attributes;
				$address->save(); 
				$order->delivery_address_id = 1; */

				/* $address = new BillingAddress();
				if($participante->billingAddress)
					$address->attributes = $participante->billingAddress->attributes;
				else
					$address->attributes = $participante->address->attributes;
				$address->save(); 
				$order->billing_address_id = 1; */
				
				foreach($cart as $position => $entrada) 
				{
					$cantidadTotal = $cantidadTotal + $entrada['amount'];
				}
				
				//$order->ordering_date = time();
				$order->ordering_date = new CDbExpression('NOW()');
				$order->payment_method = Yii::app()->user->getState('payment_method');
				$order->shipping_method = Yii::app()->user->getState('shipping_method');
				$order->comment = Yii::app()->user->getState('order_comment');
				$order->montoTotal = Shop::getJustPriceTotal();
				$order->cantidadTotal = $cantidadTotal;


				if($order->save()) {
					foreach($cart as $position => $entrada) {
						$position = new OrderPosition;
						$position->order_id = $order->order_id;
						$position->product_id = $entrada['idt_entrada'];
						$position->amount = $entrada['amount'];
						$position->specifications = @json_encode($product['Variations']);
						$position->save();
						Yii::app()->user->setState('cart', array());
						Yii::app()->user->setState('shipping_method', null);
						Yii::app()->user->setState('payment_method', null);
						Yii::app()->user->setState('order_comment', null);
					}
					Shop::mailNotification($order);
					//$this->redirect('https://sci.libertyreserve.com/en?lr_acc=U5025333&lr_amnt='.Shop::getJustPriceTotal().'&lr_currency=LRUSD&lr_fail_url=http%3a%2f%2flocalhost%2ftesis2%2findex.php%2feventos%2fshop%2forder%2ffailure&lr_fail_url_method=POST');
					//$this->redirect('https://sci.libertyreserve.com/en?lr_acc=U5356558&lr_store=BoliStore&lr_amnt=100.00&lr_currency=LRUSD&lr_status_url=http%3a%2f%2flocalhost%2ftesis2%2fconfirmacion.php&lr_status_url_method=POST');
					
					//$this->render('confirmar');
					$this->redirect(Shop::module()->successAction); //original
				} else 
					$this->redirect(Shop::module()->failureAction);
			} else {
				Shop::setFlash(
						Shop::t(
							'Por favor acepte nuestros Terminos y Condiciones para continuar'/*'Please accept our Terms and Conditions to continue'*/));
				$this->redirect(array('//eventos/shop/order/create')); //original
			}
		/*}
		else
			throw new CHttpException(4015,'Hola '.Yii::app()->user->getState('csrf', @$_POST['Order']['lr_acc']));
		*/
		//echo CHtml::endForm();
		}
		else
			throw new CHttpException(40015,Yii::t('yii','El token CSRF no pudo ser verificado.'));
	}

	public function actionSuccess()
	{
		$this->render('/order/success');
	}

	public function actionFailure()
	{
		$this->render('/order/failure');
	}


	public function actionAdmin()
	{
		$model=new Order('search');
		if(isset($_GET['Order']))
			$model->attributes=$_GET['Order'];

		$this->render('admin',array(
					'model'=>$model,
					));
	}

	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Order::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

}

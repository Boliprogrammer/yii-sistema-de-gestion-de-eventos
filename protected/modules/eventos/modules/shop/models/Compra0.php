<?php

/**
 * This is the model class for table "t_compra".
 *
 * The followings are the available columns in table 't_compra':
 * @property string $idt_compra
 * @property double $compra_montoTotal
 * @property string $compra_cantidadTotal
 * @property string $compra_fecha
 * @property string $compra_idt_participante
 *
 * The followings are the available model relations:
 * @property Participante $compraIdtParticipante
 * @property Detallecompra[] $detallecompras
 */
class Compra extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Compra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_compra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('compra_montoTotal, compra_cantidadTotal, compra_fecha, compra_idt_participante', 'required'),
			array('compra_montoTotal', 'numerical'),
			array('compra_cantidadTotal, compra_idt_participante', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idt_compra, compra_montoTotal, compra_cantidadTotal, compra_fecha, compra_idt_participante', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'compraIdtParticipante' => array(self::BELONGS_TO, 'Participante', 'compra_idt_participante'), // por defecto
			'participante' => array(self::BELONGS_TO, 'Participante', 'compra_idt_participante'),
			'detallecompras' => array(self::HAS_MANY, 'Detallecompra', 'detalleCompra_idt_compra'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idt_compra' => 'Idt Compra',
			'compra_montoTotal' => 'Compra Monto Total',
			'compra_cantidadTotal' => 'Compra Cantidad Total',
			'compra_fecha' => 'Compra Fecha',
			'compra_idt_participante' => 'Compra Idt Participante',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idt_compra',$this->idt_compra,true);
		$criteria->compare('compra_montoTotal',$this->compra_montoTotal);
		$criteria->compare('compra_cantidadTotal',$this->compra_cantidadTotal,true);
		$criteria->compare('compra_fecha',$this->compra_fecha,true);
		$criteria->compare('compra_idt_participante',$this->compra_idt_participante,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php 
?>
<div class="view">
    <h3>
		<?php echo CHtml::link(CHtml::encode($data->entrada_nombre/*$data->title*/), array('entrada/view', 'id' => $data->idt_entrada/*$data->product_id*/)); ?>
    </h3>
    
    <!-- <div class="product-overview-image">	
          	<?php 
		  	if($data->images){
		   		$this->renderPartial('/image/view', array('thumb' =>true, 'model' => $data->images[0]));
			}else {
				echo CHtml::image(Shop::register('no-pic.jpg'));
			}?>
	</div> -->
    
     <div class="product-overview-description">
        <!-- <p> <?php echo CHtml::encode($data->entrada_nombre/*como mi entrada no incluye descripcion lo puse el nombre de la entrada por convencion, pero puede ir el n�mero de entradas disponibles*//*$data->description*/); ?> </p> -->
        <p><strong> <?php echo Shop::priceFormat($data->entrada_precio/*$data->price*/); ?></strong> <br />
        <p><?php echo Shop::pricingInfo(); ?></p>
      
        <p><?php echo CHtml::link(Shop::t('Mostrar la entrada'), array('entrada/view', 'id' => $data->idt_entrada/*$data->product_id*/), array('class' =>'show-product')); ?></p>
    </div>
    
    <div class="clear"></div>
</div>
<div class="view-bottom"></div>

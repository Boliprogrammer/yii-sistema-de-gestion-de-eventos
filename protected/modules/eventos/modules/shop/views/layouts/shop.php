<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->



		<div id="mainmenu">
		<?php
	$items = array();
	/*$items[] = array('label'=>'Home', 'url'=>array('/eventos/loginout/login'));*/
	$items[] = array('label'=>'Informacion General', 'url'=>array('/eventos/evento/view/id/'.Yii::app()->user->getState('idt_evento_actual')));
	$items[] = array('label'=>'Entradas', 'url'=>array('/eventos/shop/entrada/index'));

	/*foreach(Category::model()->findAll() as $category)
	$items[] = array(
			'label' => $category->title,
			'url' => array(
				'/eventos/shop/category/view', 'id' => $category->category_id));*/
	//$items[] = array('label'=>'Admin', 'url'=>array('/eventos/shop/shop/admin'));
	$items[] = array('label'=>'Cerrar Sesion ('.Yii::app()->user->name.')', 'url'=>array('/eventos/loginout/logout', 'token'=>Yii::app()->getRequest()->getCsrfToken()), 'visible'=>!Yii::app()->user->isGuest);
	$items[] = array('label'=>'Inscribirse (nuevo)', 'url'=>array('participante/create'), 'visible'=>Yii::app()->user->isGuest);
	$items[] = array('label'=>'Login (ya registrado)', 'url'=>array('/eventos/loginout/login'), 'visible'=>Yii::app()->user->isGuest);

 $this->widget('zii.widgets.CMenu',array(
			'items'=>$items,
		)); ?>
	</div><!-- mainmenu -->
	
		<?php /* $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); */ ?><!-- breadcrumbs -->

	<div id="content">
	<div style="float: right; max-height: 200px; width: 200px; margin: 5px;">
	<?php
	$this->widget('ShoppingCartWidget'); 
	//$this->widget('ProductCategoriesWidget'); 
	if(!Yii::app()->user->isGuest) 
		$this->widget('AdminWidget');

	?>	
	</div>

	<div style="width: 700px;">
	<?php echo $content; ?>
	</div>
	</div>

	<div style="clear: both;"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

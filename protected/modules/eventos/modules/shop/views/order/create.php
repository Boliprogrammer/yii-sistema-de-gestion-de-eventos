<?php
$this->renderPartial('/order/waypoint', array('point' => 4));

$this->breadcrumbs=array(
		Shop::t('Order')=>array('index'),
		Shop::t('New Order'),
		);
?>

<?php 
Shop::renderFlash();
echo CHtml::beginForm('https://sci.libertyreserve.com/en');
echo '<br/>';
//echo CHtml::beginForm(array('//eventos/shop/order/confirm'));
echo '<h2>'.Shop::t('Confirmacion del pedido').'</h2>';

if(Shop::getCartContent() == array())
	return false;

	// If the customer is not passed over to the view, we assume the user is 
	// logged in and we fetch the customer data from the customer table
if(!isset($participante))
	$participante = Shop::getCustomer();
	$this->renderPartial('application.modules.eventos.modules.shop.views.customer.view'/*'application.modules.shop.views.customer.view'*/, array(
				'model' => $customer,
				'hideAddress' => true,
				'hideEmail' => true));
echo '<br />';
//echo '<hr />';
 echo '<p>';
	$shipping = ShippingMethod::model()->findByPk(Yii::app()->user->getState('shipping_method'));
	echo '<strong>'.Shop::t('Metodo de entrega').': </strong>'.' '.$shipping->title.' ('.$shipping->description.')';
	echo '<br />';
	echo CHtml::link(Shop::t('Editar metodo de entrega elegido'), array(
			'//eventos/shop/shippingMethod/choose', 'order' => true));
			echo '</p>'; 
echo '<p>';
	$payment = 	PaymentMethod::model()->findByPk(Yii::app()->user->getState('payment_method'));
	echo '<strong>'.Shop::t('Metodo de pago').': </strong>'.' '.$payment->title.' ('.$payment->description.')';	
	echo '<br />';
	echo CHtml::link(Shop::t('Editar metodo de pago elegido'), array(
			'//eventos/shop/paymentMethod/choose', 'order' => true));
echo '</p>';

echo '<br>';
//echo '<hr />';

$this->renderPartial('application.modules.eventos.modules.shop.views.shoppingCart.view'/*'application.modules.shop.views.shoppingCart.view'*/); 


echo '<h3>'.Shop::t('Por favor agregue algunos comentarios de su orden aqui'/*'Please add additional comments to the order here'*/).'</h3>'; 

echo CHtml::textArea('Order[Comment]',
		@Yii::app()->user->getState('order_comment'), array('style'=>'width:600px; height:100px;padding:10px;'));
		
echo '<br /><br />';

echo '<hr />';
//$this->renderPartial(Shop::module()->termsView);

?>


<div class="row buttons">

	<?php //echo CHtml::submitButton(Shop::t('Confirmar pago'),array ('id'=>'next'), array('//eventos/shop/order/confirm')); 
		
		//echo '<method="POST">';
		//echo CHtml::hiddenField('lr_acc', 'U5356558');
		echo CHtml::hiddenField('lr_acc', 'U5025333');
		//echo CHtml::hiddenField('lr_store', 'BoliStore');
		echo CHtml::hiddenField('lr_amnt', Shop::getJustPriceTotal());
		echo CHtml::hiddenField('lr_currency', 'LRUSD');
		echo CHtml::hiddenField('lr_success_url', 'http://localhost/tesis2/index.php/eventos/shop/order/confirm'); //Pago verdadero
		//echo CHtml::hiddenField('lr_success_url', 'http://localhost/tesis2/index.php/eventos/shop/order/success');
		echo CHtml::hiddenField('lr_success_url_method', 'POST');
		
		echo CHtml::hiddenField('lr_fail_url', 'http://localhost/tesis2/index.php/eventos/shop/order/failure'); //Pago verdadero
		//echo CHtml::hiddenField('lr_fail_url', 'http://localhost/tesis2/index.php/eventos/shop/order/confirm'); //Pago falso
		echo CHtml::hiddenField('lr_fail_url_method', 'POST');
		echo CHtml::hiddenField('lr_status_url', 'http://localhost/tesis2/index.php/eventos/shop/order/confirm');
		//echo CHtml::hiddenField('lr_status_url', 'http://localhost/tesis2/confirmacion.php');
		echo CHtml::hiddenField('lr_status_url_method', 'POST');
		echo CHtml::hiddenField('YII_CSRF_TOKEN', Yii::app()->request->getCsrfToken());
		//echo CHtml::textField('YII_CSRF_TOKEN_NAME', Yii::app()->request->csrfTokenName);
		echo CHtml::submitButton(Shop::t('Confirmar pago'),array (
			'id'=>'next'
			/* 'params'=>array(
				'YII_CSRF_TOKEN'=>Yii::app()->request->getCsrfToken(),
				), */
			//'YII_CSRF_TOKEN'=>Yii::app()->request->getCsrfToken(),
			)
			//'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->getCsrfToken()),
		);
		//echo CHtml::submitButton(Shop::t('Confirmar pago con echo y onclick'),array ('onclick'=>"javascript:window.open('https://sci.libertyreserve.com/en','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');",'id'=>'next'), array('//eventos/shop/order/confirm')); 
		 
				?>
	<br>
	<!-- <form action="https://sci.libertyreserve.com/en" method="POST">
		<input type="hidden" name="lr_acc" value="U5025333">
		<input type="hidden" name="lr_amnt" value="100.00">
		<input type="hidden" name="lr_currency" value="LRUSD">
		<input type="hidden" name="lr_fail_url" value="http://localhost/tesis2/index.php/eventos/shop/order/confirm">
		<input type="hidden" name="lr_fail_url_method" value="POST">	
		<input type="submit" />
	</form> -->
</div>

<?php 
	//echo '<a href="https://sci.libertyreserve.com/en?lr_acc=U5025333&lr_amnt='.$customer->idt_participante.'&lr_currency=LRUSD" alt="Pay With Liberty Reserve!">Pay With Liberty Reserve!</a>';
	//$valor = Shop::getPriceTotal();
	//echo Shop::getJustPriceTotal();
	//echo '<a href="https://sci.libertyreserve.com/en?lr_acc=U5025333&lr_amnt='.Shop::getJustPriceTotal().'&lr_currency=LRUSD" alt="Pay With Liberty Reserve!">Pay With Liberty Reserve!</a>'
	// sirve // echo '<a href="https://sci.libertyreserve.com/en?lr_acc=U5025333&lr_amnt='.Shop::getJustPriceTotal().'&lr_currency=LRUSD&lr_fail_url=http%3a%2f%2flocalhost%2ftesis2%2findex.php%2feventos%2fshop%2forder%2ffailure&lr_fail_url_method=POST" alt="Pay With Liberty Reserve 2!">Pay With Liberty Reserve 2!</a>'
?>

<!-- <a href="https://sci.libertyreserve.com/en?lr_acc=U5025333&lr_amnt=10.00&lr_currency=LRUSD" alt="Pay With Liberty Reserve!">Pay With Liberty Reserve!</a> -->

<?php /*$this->widget('application.extensions.donatebutton.donatebutton', 
     array(
           'vertical' => false,
           'show_frame' => false,
           'biggerbutton' => false,
           'fix_amount' => true,
           'alt_button' => 'Donate',
           'business' => 'alejandrobolivar86@gmail.com',
           'item_name' => 'Support Your Business',
           'message' => 'If you like our work, you may throw us some cookies. Donate ',
       'donation' => array(
            'currency_code'=>'USR',
            'amount'=>10
        )
      )
);*/ ?>
<?php echo CHtml::endForm(); ?>
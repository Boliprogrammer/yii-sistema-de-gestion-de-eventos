<?php
$points = array(
		Shop::t(' >> Informacion Personal >> '),
		Shop::t('Entrega >> '),
		Shop::t('Pago >> '),
		Shop::t('Confirmacion >> '),
		Shop::t('Exito')
);

$links = array(
		//array('//eventos/shop/customer/create'),
		array('//eventos/participante/view/'),
		array('//eventos/shop/shippingMethod/choose'),
		array('//eventos/shop/paymentMethod/choose'),
		array('//eventos/shop/order/create'));


echo '<div id="waypointarea" class="waypointarea">';
	printf('<span class="waypoint %s">%s</span>',
			$point == 0 ? 'active' : '',
			CHtml::link(Shop::t('Carrito de compras '), array(
						'//eventos/shop/shoppingCart/view')));

foreach ($points as $p => $pointText) 
{
	printf('<span class="waypoint%s">%s</span>',
			($point == ++$p) ? ' active' : '',
			$point < ++$p ? $pointText : CHtml::link($pointText, @$links[$p-2])
			);
}
echo '</div>';
?>

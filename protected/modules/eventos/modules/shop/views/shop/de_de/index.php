<?php
$this->breadcrumbs=array(
	$this->module->id,
);
?>

<h1> Deutsch </h1>

<div class="span-8"> 
<?php $this->beginWidget('zii.widgets.CPortlet', array('title' => Yii::t('YiiShop', 'Su carrito de compras'))); ?>
<?php $this->renderPartial('/shoppingCart/index', array()); ?>
<?php $this->endWidget(); ?>

<!-- <?php /* $this->beginWidget('zii.widgets.CPortlet', array('title' => Yii::t('YiiShop', 'Categorias de Productos'))); */ ?> -->
<!-- <?php /* $this->renderPartial('/category/index'); */ ?> -->
<!-- <?php /* $this->endWidget(); */ ?> -->
</div>

<div class="span-16 last"> 
<?php $this->renderPartial('/shop/welcome'); ?>
</div>

<div style="clear:both;"> </div>


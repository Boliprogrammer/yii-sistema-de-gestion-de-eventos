<?php
$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>

<p>
This is the view content for action "<?php echo $this->action->id; ?>".
The action belongs to the controller "<?php echo get_class($this); ?>"
in the "<?php echo $this->module->id; ?>" module.
</p>
<p>
You may customize this page by editing <tt><?php echo __FILE__; ?></tt>
</p>

<h1><?php echo Yii::app()->getBaseUrl(true); ?></h1>

<p><?php echo CHtml::link("Ruta",array(Yii::app()->params['url'].Yii::app()->request->requestUri));
?></p>

<p><?php echo CHtml::link("Hello",array(Yii::app()->params['url'].Yii::app()->request->requestUri));
?></p>

<a href="index.php?r=controller/action">Link Text</a>
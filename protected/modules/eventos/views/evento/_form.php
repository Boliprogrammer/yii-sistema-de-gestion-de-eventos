<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'evento-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary(array_merge(array($model),$validatedEntradas)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_nombre'); ?>
		<?php echo $form->textField($model,'evento_nombre',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'evento_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_fecha_inicio'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
			'model'=>$model,
			'name'=>'evento_fecha_inicio',
			'options'=>array(
                'anim'=>'slide',
                'dateFormat'=>'yy-mm-dd',
                'timeFormat'=>'hh:mm:ss',
                'changeMonth'=>true,
            ),
		)); ?>
		<?php echo $form->error($model,'evento_fecha_inicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_fecha_fin'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
			'model'=>$model,
			'name'=>'evento_fecha_fin',
			'options'=>array(
                'anim'=>'slide',
                'dateFormat'=>'yy-mm-dd',
                'timeFormat'=>'hh:mm:ss',
                'changeMonth'=>true,
            ),
		)); ?>
		<?php echo $form->error($model,'evento_fecha_fin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_tipo_evento'); ?>
		<?php echo $form->dropDownList($model,'evento_tipo_evento',$model->getTypeEventOptions()); ?>
		<?php echo $form->error($model,'evento_tipo_evento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_lugar'); ?>
		<?php echo $form->textField($model,'evento_lugar',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'evento_lugar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_cupo_maximo'); ?>
		<?php echo $form->textField($model,'evento_cupo_maximo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'evento_cupo_maximo'); ?>
	</div>

	<?php
 
// see http://www.yiiframework.com/doc/guide/1.1/en/form.table
// Note: Can be a route to a config file too,
//       or create a method 'getMultiModelForm()' in the member model
 
	$entradaFormConfig = array(
		  'elements'=>array(
			'entrada_nombre'=>array(
				'type'=>'text',
				'maxlength'=>40,
			),
			'entrada_precio'=>array(
				'type'=>'text',
				'maxlength'=>40,
			),
			'entrada_cantidadMaxima'=>array(
				'type'=>'text',
				'maxlength'=>40,
			),
		));
	 
	$this->widget('ext.multimodelform.MultiModelForm',array(
			'id' => 'id_entrada', //the unique widget id
			'formConfig' => $entradaFormConfig, //the form configuration array
			'model' => $entrada, //instance of the form model
	 
			//if submitted not empty from the controller,
			//the form will be rendered with validation errors
			'validatedItems' => $validatedEntradas,
	 
			//array of entrada instances loaded from db
			'data' => $entrada->findAll('entrada_idt_evento=:eventoId', array(':eventoId'=>$model->idt_evento)),
		));
	?>
	 
		
	<div class="row">
		<?php echo $form->labelEx($model,'evento_categoria'); ?>
		<?php echo $form->dropDownList($model,'evento_categoria',$model->getCategoryOptions()); ?>
		<?php echo $form->error($model,'evento_categoria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_descripcion'); ?>
		<?php echo $form->textArea($model,'evento_descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'evento_descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evento_estado'); ?>
		<?php echo $form->dropDownList($model,'evento_estado',$model->getStateOptions()); ?>
		<?php echo $form->error($model,'evento_estado'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'evento_idt_organizador'); ?>
		<?php echo $form->textField($model,'evento_idt_organizador',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'evento_idt_organizador'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Modificar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
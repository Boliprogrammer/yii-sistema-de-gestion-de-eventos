<?php
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar eventos', 'url'=>array('index')),
	array('label'=>'Buscar evento', 'url'=>array('admin')),
);
?>

<h1>Cree su propio evento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,
								'entrada'=>$entrada,'validatedEntradas'=>$validatedEntradas)); ?>
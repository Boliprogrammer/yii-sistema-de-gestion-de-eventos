<?php
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	$model->idt_evento=>array('view','id'=>$model->idt_evento),
	'Update',
);

$this->menu=array(
	array('label'=>'List Evento', 'url'=>array('index')),
	array('label'=>'Create Evento', 'url'=>array('create')),
	array('label'=>'View Evento', 'url'=>array('view', 'id'=>$model->idt_evento)),
	array('label'=>'Manage Evento', 'url'=>array('admin')),
);
?>

<h1>Update Evento <?php echo $model->idt_evento; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
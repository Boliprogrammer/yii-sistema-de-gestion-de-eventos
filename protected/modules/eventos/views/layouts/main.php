<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name) . " - Eventos"; ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				//array('label'=>'Back To Main Site', 'url'=>array('/site/login'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Informacion General', 'url'=>array('/eventos/evento/index'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Inscribirse', 'url'=>array('/participante/create'), 'visible'=>(Yii::app()->getModule('eventos')->user->isGuest || Yii::app()->user->isGuest)),
				array('label'=>'Informacion General', 'url'=>array('/eventos/evento/view/id/'.Yii::app()->user->getState('idt_evento_actual'))),
				array('label'=>'Entradas', 'url'=>array('/eventos/shop/entrada/index'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Admin', 'url'=>array('/eventos/shop/shop/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Registrarse', 'url'=>array('participante/create'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Mi Cuenta', 'url'=>array('/admin/organizador/index'), 'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Mi Cuenta', 'url'=>array('organizador/view', 'id'=>Yii::app()->user->id)),
				//array('label'=>'Registrarse', 'url'=>array('/organizador/create')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				//array('label'=>'Registrarse', 'url'=>array('/organizador/create')),
				array('label'=>'Login', 'url'=>array('/eventos/loginout/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Cerrar Sesion ('.Yii::app()->getModule('eventos')->user->name.')', 'url'=>array('/default/logout'), 'visible'=>(!Yii::app()->getModule('eventos')->user->isGuest || !Yii::app()->user->isGuest))
				
				array('label'=>'Cerrar Sesion ('.Yii::app()->user->name.')', 'url'=>array('/eventos/loginout/logout', 'token'=>Yii::app()->getRequest()->getCsrfToken()), 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by OPC Eventos Intelecturales y Corporativos.<br/>
		Todos los Derechos Reservados.<br/>
		Powered by BoliBar.
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

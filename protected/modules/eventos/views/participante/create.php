<?php
$this->breadcrumbs=array(
	'Participante'=>array('index'),
	'Registro',
);

$this->menu=array(/*
	array('label'=>'List Participante', 'url'=>array('index')),
	array('label'=>'Manage Participante', 'url'=>array('admin')),*/
);
?>

<h1>Registro Participante</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Participantes'=>array('index'),
	$model->idt_participante=>array('view','id'=>$model->idt_participante),
	'Update',
);

$this->menu=array(
	array('label'=>'List Participante', 'url'=>array('index')),
	array('label'=>'Create Participante', 'url'=>array('create')),
	array('label'=>'View Participante', 'url'=>array('view', 'id'=>$model->idt_participante)),
	array('label'=>'Manage Participante', 'url'=>array('admin')),
);
?>

<h1>Update Participante <?php echo $model->idt_participante; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
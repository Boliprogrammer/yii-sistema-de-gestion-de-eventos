<?php
$this->breadcrumbs=array(
	'Participantes'=>array('index'),
	$model->participante_nombre.' '.$model->participante_apellido,
);

/* $this->menu=array(
	array('label'=>'List Participante', 'url'=>array('index')),
	array('label'=>'Create Participante', 'url'=>array('create')),
	array('label'=>'Update Participante', 'url'=>array('update', 'id'=>$model->idt_participante)),
	array('label'=>'Delete Participante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idt_participante),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Participante', 'url'=>array('admin')),
); */
?>


<h1>Tus datos personales como Participante <?php /*echo $model->idt_participante;*/?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idt_participante',
		'participante_nombre',
		'participante_apellido',
		'participante_correo',
		//'participante_clave',
		'participante_creacion',
		//'participante_modificacion',
	),
)); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'organizador-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_nombre'); ?>
		<?php echo $form->textField($model,'organizador_nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'organizador_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_apellido'); ?>
		<?php echo $form->textField($model,'organizador_apellido',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'organizador_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_empresa'); ?>
		<?php echo $form->textField($model,'organizador_empresa',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'organizador_empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_correo'); ?>
		<?php echo $form->textField($model,'organizador_correo',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'organizador_correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_clave'); ?>
		<?php echo $form->passwordField($model,'organizador_clave',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'organizador_clave'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'repetir clave');?>
		<?php echo $form->passwordField($model,'organizador_clave_repeat',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'organizador_clave_repeat'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'organizador_creacion'); ?>
		<?php echo $form->textField($model,'organizador_creacion'); ?>
		<?php echo $form->error($model,'organizador_creacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organizador_modificacion'); ?>
		<?php echo $form->textField($model,'organizador_modificacion'); ?>
		<?php echo $form->error($model,'organizador_modificacion'); ?>
	</div> -->
	
	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
			<div>
				<?php $this->widget('CCaptcha'); ?>
				<?php echo $form->textField($model,'verifyCode'); ?>
			</div>
			<div class="hint">Por favor ingresa las letras como se muestran.
			<br/>Letras no son sensibles.</div>
			<?php echo $form->error($model,'verifyCode');  ?>
		</div>
    <?php endif; ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrarse' : 'Actualizar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
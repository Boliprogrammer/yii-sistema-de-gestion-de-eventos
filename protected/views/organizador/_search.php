<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idt_organizador'); ?>
		<?php echo $form->textField($model,'idt_organizador',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_nombre'); ?>
		<?php echo $form->textField($model,'organizador_nombre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_apellido'); ?>
		<?php echo $form->textField($model,'organizador_apellido',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_empresa'); ?>
		<?php echo $form->textField($model,'organizador_empresa',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_correo'); ?>
		<?php echo $form->textField($model,'organizador_correo',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_clave'); ?>
		<?php echo $form->textField($model,'organizador_clave',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_creacion'); ?>
		<?php echo $form->textField($model,'organizador_creacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizador_modificacion'); ?>
		<?php echo $form->textField($model,'organizador_modificacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
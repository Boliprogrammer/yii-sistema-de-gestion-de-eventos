<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idt_organizador')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idt_organizador), array('view', 'id'=>$data->idt_organizador)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organizador_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organizador_apellido')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organizador_empresa')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_empresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organizador_correo')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_correo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organizador_clave')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_clave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->organizador_creacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	*/ ?>

</div>
<?php
$this->breadcrumbs=array(
	'Organizadors',
);

$this->menu=array(
	array('label'=>'Create Organizador', 'url'=>array('create')),
	array('label'=>'Manage Organizador', 'url'=>array('admin')),
);
?>

<h1>Organizadors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

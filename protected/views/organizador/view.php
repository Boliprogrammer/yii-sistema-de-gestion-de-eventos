<?php
$this->breadcrumbs=array(
	//'Organizador'=>array('index'),
	$model->organizador_nombre.' '.$model->organizador_apellido,
);

$this->menu=array(
	//array('label'=>'List Organizador', 'url'=>array('index')),
	//array('label'=>'Create Organizador', 'url'=>array('create')),
	array('label'=>'Actualizar datos', 'url'=>array('update'/*, 'id'=>$model->idt_organizador*/)),
	array('label'=>'Cancelar Cuenta', 'url'=>array('/site/logout'), 'linkOptions'=>array(
																		'submit'=>array('delete'/*,'id'=>$model->idt_organizador*/),
																		'confirm'=>'Esta seguro que quiere cancelar su cuenta para siempre?',
																		'csrf'=>true,
																	)
																),
	//array('label'=>'Manage Organizador', 'url'=>array('admin')),
);
?>

<h1>Organizador: <?php echo $model->organizador_nombre . ' ' . $model->organizador_apellido; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idt_organizador',
		'organizador_nombre',
		'organizador_apellido',
		'organizador_empresa',
		'organizador_correo',
		//'organizador_clave',
		//'organizador_creacion',
		//'organizador_modificacion',
	),
)); ?>

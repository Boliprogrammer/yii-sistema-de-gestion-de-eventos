<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'participante-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'participante_nombre'); ?>
		<?php echo $form->textField($model,'participante_nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'participante_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'participante_apellido'); ?>
		<?php echo $form->textField($model,'participante_apellido',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'participante_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'participante_correo'); ?>
		<?php echo $form->textField($model,'participante_correo',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'participante_correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'participante_clave'); ?>
		<?php echo $form->passwordField($model,'participante_clave',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'participante_clave'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'participante_clave_repeat');?>
		<?php echo $form->passwordField($model,'participante_clave_repeat',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'participante_clave_repeat'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'participante_creacion'); ?>
		<?php echo $form->textField($model,'participante_creacion'); ?>
		<?php echo $form->error($model,'participante_creacion'); ?>
	</div> -->

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'participante_modificacion'); ?>
		<?php echo $form->textField($model,'participante_modificacion'); ?>
		<?php echo $form->error($model,'participante_modificacion'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
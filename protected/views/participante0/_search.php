<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idt_participante'); ?>
		<?php echo $form->textField($model,'idt_participante',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_nombre'); ?>
		<?php echo $form->textField($model,'participante_nombre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_apellido'); ?>
		<?php echo $form->textField($model,'participante_apellido',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_correo'); ?>
		<?php echo $form->textField($model,'participante_correo',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_clave'); ?>
		<?php echo $form->textField($model,'participante_clave',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_creacion'); ?>
		<?php echo $form->textField($model,'participante_creacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participante_modificacion'); ?>
		<?php echo $form->textField($model,'participante_modificacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
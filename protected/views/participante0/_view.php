<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idt_participante')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idt_participante), array('view', 'id'=>$data->idt_participante)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->participante_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_apellido')); ?>:</b>
	<?php echo CHtml::encode($data->participante_apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_correo')); ?>:</b>
	<?php echo CHtml::encode($data->participante_correo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_clave')); ?>:</b>
	<?php echo CHtml::encode($data->participante_clave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_creacion')); ?>:</b>
	<?php echo CHtml::encode($data->participante_creacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participante_modificacion')); ?>:</b>
	<?php echo CHtml::encode($data->participante_modificacion); ?>
	<br />


</div>
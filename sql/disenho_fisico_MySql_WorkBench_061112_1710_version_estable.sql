CREATE DATABASE  IF NOT EXISTS `tesis2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tesis2`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: tesis2
-- ------------------------------------------------------
-- Server version	5.5.8

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_campanha`
--

DROP TABLE IF EXISTS `t_campanha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_campanha` (
  `idt_campanha` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campanha_nombre` varchar(200) NOT NULL,
  `campanha_asunto` varchar(200) NOT NULL,
  `campanha_mensaje` text,
  `campanha_idt_evento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_campanha`),
  KEY `fk_campanha_evento` (`campanha_idt_evento`),
  CONSTRAINT `fk_campanha_evento` FOREIGN KEY (`campanha_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_campanha`
--

LOCK TABLES `t_campanha` WRITE;
/*!40000 ALTER TABLE `t_campanha` DISABLE KEYS */;
INSERT INTO `t_campanha` VALUES (1,'Nombre de la campaña de alejandrobolivar86@gmail.com del evento id 8','Asunto del mensaje','',8),(2,'Nombre de la campaña','Asunto del mensaje','Mensaje',8),(3,'Nombre de la campaña','Asunto del mensaje','Mensaje',10),(4,'Campaña de Prueba 1','Asunto de la campaña de prueba 1','Mensaje',8),(5,'Campaña solo con Nombre y Asunto','Asunto','',8),(6,'Nombre de la campaña','Asunto de la campanha larga','Mensaje',15),(7,'Nombre de la campaña','Asunto','A',16),(8,'Nombre de la campaña','Asunto de la campaña','Mensaje',21),(9,'Campaña cualquiera 2','Asunto','',21),(10,'Campaña de mark Zuckerber','Asunto','',35);
/*!40000 ALTER TABLE `t_campanha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_compra`
--

DROP TABLE IF EXISTS `t_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_compra` (
  `idt_compra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `compra_montoTotal` float(11,4) unsigned NOT NULL,
  `compra_cantidadTotal` int(10) unsigned NOT NULL,
  `compra_fecha` datetime NOT NULL,
  `compra_idt_participante` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_compra`),
  KEY `fk_compra_participante` (`compra_idt_participante`),
  CONSTRAINT `fk_compra_participante` FOREIGN KEY (`compra_idt_participante`) REFERENCES `t_participante` (`idt_participante`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_compra`
--

LOCK TABLES `t_compra` WRITE;
/*!40000 ALTER TABLE `t_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contacto`
--

DROP TABLE IF EXISTS `t_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_contacto` (
  `idt_contacto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contacto_nombre` varchar(100) NOT NULL,
  `contacto_apellido` varchar(100) NOT NULL,
  `contacto_correo` varchar(150) NOT NULL,
  `contacto_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_contacto`),
  KEY `fk_contacto_organizador` (`contacto_idt_organizador`),
  CONSTRAINT `fk_contacto_organizador` FOREIGN KEY (`contacto_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contacto`
--

LOCK TABLES `t_contacto` WRITE;
/*!40000 ALTER TABLE `t_contacto` DISABLE KEYS */;
INSERT INTO `t_contacto` VALUES (1,'Carlos','Medina','carlosmedina@gmail.com',1),(3,'Lady','Gaga','ladygaga@gmail.com',1),(4,'Larry','Ellison','larryellison@gmail.com',1),(7,'Eike','Batista','eikebatista@gmail.com',1),(8,'Bill','Gates','billgates@gmail.com',5),(10,'Bon','Jovi','bonjovi@gmail.com',8),(12,'Bill','Gates','billgates@gmail.com',8),(13,'Bill','Gates','billgates@gmail.com',1),(14,'Carlos','Medina','carlosmedina@gmail.com',5),(19,'Pitbull','Pitbull','pitbull@gmail.com',4),(20,'Pitbull','Pitbull','pitbull@gmail.com',1),(21,'Pepe','Mosca','pepemosca@gmail.com',19);
/*!40000 ALTER TABLE `t_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_detallecompra`
--

DROP TABLE IF EXISTS `t_detallecompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_detallecompra` (
  `idt_detalleCompra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detalleCompra_precio` float(11,4) unsigned NOT NULL,
  `detalleCompra_cantidad` int(10) unsigned NOT NULL,
  `detalleCompra_idt_compra` int(10) unsigned NOT NULL,
  `detalleCompra_idt_entrada` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_detalleCompra`),
  KEY `fk_detalleCompra_compra` (`detalleCompra_idt_compra`),
  KEY `fk_detalleCompra_entrada` (`detalleCompra_idt_entrada`),
  CONSTRAINT `fk_detalleCompra_compra` FOREIGN KEY (`detalleCompra_idt_compra`) REFERENCES `t_compra` (`idt_compra`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleCompra_entrada` FOREIGN KEY (`detalleCompra_idt_entrada`) REFERENCES `t_entrada` (`idt_entrada`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_detallecompra`
--

LOCK TABLES `t_detallecompra` WRITE;
/*!40000 ALTER TABLE `t_detallecompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_detallecompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_entrada`
--

DROP TABLE IF EXISTS `t_entrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_entrada` (
  `idt_entrada` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entrada_nombre` varchar(150) NOT NULL,
  `entrada_precio` float(11,4) unsigned NOT NULL DEFAULT '0.0000',
  `entrada_cantidadMaxima` int(10) unsigned DEFAULT '0',
  `entrada_idt_evento` int(10) unsigned NOT NULL,
  `entrada_tax_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idt_entrada`),
  UNIQUE KEY `idt_entrada_UNIQUE` (`idt_entrada`),
  KEY `fk_entrada_evento` (`entrada_idt_evento`),
  KEY `fk_entrada_tax_idx` (`entrada_tax_id`),
  CONSTRAINT `fk_entrada_evento` FOREIGN KEY (`entrada_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_entrada_tax` FOREIGN KEY (`entrada_tax_id`) REFERENCES `t_shop_tax` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_entrada`
--

LOCK TABLES `t_entrada` WRITE;
/*!40000 ALTER TABLE `t_entrada` DISABLE KEYS */;
INSERT INTO `t_entrada` VALUES (1,'Estándar',100.0000,1000,27,NULL),(2,'Estándar',100.0000,100,29,NULL),(3,'VIP',300.0000,10,29,NULL),(4,'Estándar',100.0000,100,30,NULL),(5,'Butaca',100.0000,1000,31,NULL),(6,'Curva',50.0000,10000,31,NULL),(7,'Extranjeros',904.8000,1000,32,NULL),(8,'Gold',2500.9900,10,33,NULL),(9,'Standar',15.0000,100,36,NULL),(10,'Hombres',350.0000,600,37,NULL),(11,'Mujeres',250.0000,1000,37,NULL),(12,'Publico en General',30.0000,150,39,NULL),(13,'VIP',10000.0000,50000,41,NULL),(14,'Publico en General',1000.0000,15000,42,NULL),(15,'General',140.0000,10000,43,NULL),(16,'Cancha',280.0000,10000,43,NULL),(17,'VIP',700.0000,10001,43,NULL),(18,'General',350.0000,10000,44,NULL),(19,'Estudiantes',250.0000,10000,44,NULL),(20,'Curva',50.0000,10,46,NULL),(21,'Estudiantes',150.0000,100,48,NULL),(22,'Externo',250.0000,50,48,NULL);
/*!40000 ALTER TABLE `t_entrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_evento`
--

DROP TABLE IF EXISTS `t_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_evento` (
  `idt_evento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `evento_nombre` varchar(150) NOT NULL,
  `evento_fecha_inicio` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_fecha_fin` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_tipo_evento` bit(1) NOT NULL DEFAULT b'0',
  `evento_lugar` varchar(150) DEFAULT NULL,
  `evento_cupo_maximo` int(10) unsigned DEFAULT '0',
  `evento_categoria` int(3) unsigned DEFAULT NULL,
  `evento_descripcion` text,
  `evento_estado` int(1) unsigned NOT NULL DEFAULT '0',
  `evento_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_evento`),
  KEY `fk_evento_organizador` (`evento_idt_organizador`),
  CONSTRAINT `fk_evento_organizador` FOREIGN KEY (`evento_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_evento`
--

LOCK TABLES `t_evento` WRITE;
/*!40000 ALTER TABLE `t_evento` DISABLE KEYS */;
INSERT INTO `t_evento` VALUES (2,'Proyecto de Grado I',NULL,'2012-07-20 10:06:17','','',0,4,'',1,7),(3,'Marketing II',NULL,'2012-07-21 00:00:00','','',0,0,'',0,7),(4,'Ingeniería de Software II','2012-07-20 10:10:26','2012-07-20 10:10:28','\0','',0,0,'',0,7),(5,'Congreso de Cientificos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,7),(6,'Congreso de Finanzas Personales','2012-07-20 10:22:54','2012-07-20 10:22:56','\0','',0,0,'',0,7),(7,'Congreso Bolmun','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',2000,0,'',0,7),(8,'evento de alejandrobolivar86@gmail.com','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(9,'Congreso de Psiologia','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(10,'Congresos de Informáticos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(11,'Seminario de Ventas','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(12,'Lorgio Serrate\'s Event','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(13,'Bill Gate\'s Evento','2012-07-23 10:28:53','2012-07-23 10:28:55','','Bill Gate\'s House',5000,4,'Release Windwos 8',0,1),(14,'Karl Albrecht\'\'s Event','2012-07-23 10:33:30','2012-07-23 10:33:32','\0','',0,1,'',0,1),(15,'Windows 8 Release','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',100000,0,'Especificaciones',0,5),(16,'Evento 2','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,5),(20,'asdfasd','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,18),(21,'Cata de Vino','2012-07-19 10:06:17','2012-07-28 10:06:17','\0','',0,0,'',0,19),(22,'Evento con Entradas habilitado pero sin niguna entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(23,'Evento con 2 tipos de entradas','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(24,'Evento con 1 tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(25,'Evento con 1 tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(26,'Evento con un tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(27,'Evento con un tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(28,'Evento con un tipo de entrada y con cantidad maxima obligatorio de entrada vacio','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(29,'Evento con 2 tipos de entradas y todos sus campos son obligatorios','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(30,'Prueba 1 para el caso de uso Crear Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(31,'Prueba 2 para el caso de uso Crear Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(32,'Caso de prueba 3 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(33,'Prueba 5 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(34,'Prueba 6 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(35,'Evento1','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,20),(36,'Eventosss','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(37,'Sucre 2012-2013','2012-12-31 22:00:00','2013-01-01 07:00:00','','Mooy',1600,0,'Nuevamente...',0,19),(38,'After Parciales UPSA 2.0 (BoliBar)','2012-11-10 14:00:00','2012-11-11 04:00:00','','BoliBar',150,1,'Se los espera...',0,19),(39,'After Parciales UPSA 2.0 (BoliBar)','2012-11-10 14:00:00','2012-11-11 05:00:00','\0','BoliBar',150,1,'Se los espera nuevamente...',0,19),(40,'Google 3.0 Release Candidate','2012-12-15 09:00:00','2012-12-16 17:00:00','','Silicon Valley',30000,0,'Be prepared for the next explosion of the net...',1,20),(41,'Google 4.0 Release Candidate','2012-12-15 09:00:00','2012-12-16 17:00:00','','Silicon Valley',50000,4,'Be prepared for the next explosion of the net...',1,20),(42,'Facebook 3.0','2012-10-20 21:50:57','2012-10-20 21:50:59','\0','Silicon Valley',15000,0,'The next big bang...',0,20),(43,'Concierto de Pitbull','2012-10-21 21:25:45','2012-10-21 21:25:47','','Tahuichi Aguilera',30000,4,'Conciertaaaaaaaaaaazo...',0,19),(44,'Congreso Conandino 2012 UPSA','2012-10-22 10:06:17','2012-10-22 10:06:17','\0','Salon de Convenciones - UPSA',1500,1,'Bienvenidos a la 12va versión...',0,19),(45,'Evento con la carpeta \"admin\\views\\entrada0\"','2012-10-22 14:31:42','2012-10-22 14:31:44','\0','Cualquiera',100,2,'se cambio el nombre de la carpeta \"entrada\" a \"entrada0\" para verificar si esa carpeta se esta utilizando o no o que es lo mismo si sale error o no.',0,19),(46,'Evento con la carpeta \"admin\\views\\entrada0\" Nro. 2 con Entadas','2012-10-22 14:56:42','2012-10-22 14:56:45','\0','Whatever',10,0,'se cambio el nombre de la carpeta \"entrada\" a \"entrada0\" para verificar si esa carpeta se esta utilizando o no o que es lo mismo si sale error o no.\r\n\r\nEs la segunda prueba pero esta vez se agregó una entrada.',0,19),(47,'Evento de Defensa','2012-10-23 19:19:41','2012-10-23 19:19:39','\0','UPSA',100,0,'Hola...',0,19),(48,'Evento de Defensa 2','2012-10-23 19:22:14','2012-10-23 19:22:16','\0','UPSA',100,0,'',0,19);
/*!40000 ALTER TABLE `t_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_inscripcion`
--

DROP TABLE IF EXISTS `t_inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_inscripcion` (
  `inscripcion_idt_participante` int(10) unsigned NOT NULL,
  `inscripcion_idt_evento` int(10) unsigned NOT NULL,
  `inscripcion_fecha` datetime NOT NULL,
  PRIMARY KEY (`inscripcion_idt_participante`,`inscripcion_idt_evento`),
  KEY `fk_inscripcion_participante` (`inscripcion_idt_participante`),
  KEY `fk_inscripcion_evento` (`inscripcion_idt_evento`),
  CONSTRAINT `fk_inscripcion_evento` FOREIGN KEY (`inscripcion_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_inscripcion_participante` FOREIGN KEY (`inscripcion_idt_participante`) REFERENCES `t_participante` (`idt_participante`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_inscripcion`
--

LOCK TABLES `t_inscripcion` WRITE;
/*!40000 ALTER TABLE `t_inscripcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_invitacion`
--

DROP TABLE IF EXISTS `t_invitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_invitacion` (
  `idt_invitacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invitacion_correos` text NOT NULL,
  `invitacion_cantidad` int(5) DEFAULT NULL,
  `invitacion_resultado` int(11) DEFAULT NULL,
  `invitacion_idt_campanha` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_invitacion`),
  KEY `fk_invitacion_campanha` (`invitacion_idt_campanha`),
  CONSTRAINT `fk_invitacion_campanha` FOREIGN KEY (`invitacion_idt_campanha`) REFERENCES `t_campanha` (`idt_campanha`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_invitacion`
--

LOCK TABLES `t_invitacion` WRITE;
/*!40000 ALTER TABLE `t_invitacion` DISABLE KEYS */;
INSERT INTO `t_invitacion` VALUES (1,'alejandrobolivar86@gmail.com',NULL,NULL,8),(2,'alejandrobolivar86@gmail.com',NULL,NULL,9),(3,'alejandrobolivar86@gmail.com, alejandrobolivar@hotmail.com',NULL,NULL,9),(4,'alejandrobolivar86@gmail.com',NULL,NULL,10),(5,'alejandrobolivar86@gmail.com',NULL,NULL,10),(6,'alejandrobolivar86@gmail.com',NULL,NULL,10),(7,'alejandrobolivar@hotmail.com',NULL,NULL,10),(8,'alejandrobolivar86@gmail.com',NULL,NULL,10),(9,'alejandrobolivar86@gmail.com',NULL,NULL,10),(10,'alejandrobolivar@hotmail.com',NULL,NULL,10),(11,'alejandrobolivar86@gmail.com',NULL,NULL,10),(12,'alejandrobolivar86@gmail.com',NULL,NULL,10),(13,'alejandrobolivar@hotmail.com',NULL,NULL,10),(14,'alejandrobolivar86@gmail.com',NULL,NULL,10),(15,'alejandrobolivar@hotmail.com',NULL,NULL,10),(16,'alejandrobolivar86@gmail.com',NULL,NULL,10),(17,'alejandrobolivar86@gmail.com',NULL,NULL,10),(18,'alejandrobolivar86@gmail.com',NULL,NULL,10),(19,'alejandrobolivar86@gmail.com',NULL,NULL,10),(20,'alejandrobolivar86@gmail.com',NULL,NULL,10),(21,'alejandrobolivar86@gmail.com',NULL,NULL,10),(22,'alejandrobolivar86@gmail.com',NULL,NULL,10),(23,'alejandrobolivar86@gmail.com',NULL,NULL,1);
/*!40000 ALTER TABLE `t_invitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_organizador`
--

DROP TABLE IF EXISTS `t_organizador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_organizador` (
  `idt_organizador` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organizador_nombre` varchar(100) NOT NULL,
  `organizador_apellido` varchar(100) NOT NULL,
  `organizador_empresa` varchar(150) DEFAULT NULL,
  `organizador_correo` varchar(150) NOT NULL,
  `organizador_clave` varchar(50) NOT NULL,
  `organizador_creacion` datetime NOT NULL,
  `organizador_modificacion` datetime NOT NULL,
  PRIMARY KEY (`idt_organizador`),
  UNIQUE KEY `organizador_correo_UNIQUE` (`organizador_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_organizador`
--

LOCK TABLES `t_organizador` WRITE;
/*!40000 ALTER TABLE `t_organizador` DISABLE KEYS */;
INSERT INTO `t_organizador` VALUES (1,'Alejandro','Bolivar','OPC Eventos Intelectuales Operativos','alejandrobolivar86@gmail.com','e052450f29b2e0e9a53fd4eb389e25a9','2012-07-16 15:19:04','2012-07-23 19:58:48'),(2,'Made','Jacobs','UPSA','madejacobs@gmail.com','made','2012-07-16 15:52:45','2012-07-16 15:52:45'),(3,'Fernando','Chavez','YPFB','fernandochavez@gmail.com','cebdd715d4ecaafee8f147c2e85e0754','2012-07-16 16:54:06','2012-07-16 16:54:06'),(4,'Carlos','Slim','Telecom','carlosslim@gmail.com','dc599a9972fde3045dab59dbd1ae170b','2012-07-17 14:37:08','2012-07-17 14:37:08'),(5,'Bill','Gates','Microsoft','billgates@gmail.com','e8375d7cd983efcbf956da5937050ffc','2012-07-17 14:37:50','2012-07-23 20:45:46'),(7,'Warren','Buffett','Berkshire Hathaway','warrenbuffett@gmail.com','80ee7ece5bdf991bc2ae95776f02568d','2012-07-19 17:38:57','2012-07-19 17:38:57'),(8,'Lorgio','Serrate','Muebles Metálicos Bolivar','lorgioserrate@gmail.com','cbe32f85911a867690f8324cd91737c2','2012-07-20 18:38:56','2012-07-23 18:56:23'),(18,'Michael','Mind','','michaelmind@gmail.com','0acf4539a14b3aa27deeb4cbdf6e989f','2012-07-26 15:17:37','2012-07-26 15:17:37'),(19,'Oscar','Crespo','','oscarcrespo@gmail.com','f156e7995d521f30e6c59a3d6c75e1e5','2012-07-28 17:15:58','2012-07-28 17:15:58'),(20,'Mark','Zuckerberg','','markzuckerberg@gmail.com','ea82410c7a9991816b5eeeebe195e20a','2012-07-31 12:56:38','2012-07-31 12:56:38'),(21,'Juan','Magan','','juanmagan@gmail.com','a94652aa97c7211ba8954dd15a3cf838','2012-08-01 11:45:52','2012-08-01 11:45:52'),(22,'Larry','Page','Google','larrypage@gmail.com','66f4b449b3a98abf87f2521e35513542','2012-10-20 14:56:56','2012-10-20 14:56:56'),(23,'Solo','Organizador','Alguna','soloorganizador@gmail.com','5653c6b1f51852a6351ec69c8452abc6','2012-10-30 12:07:00','2012-10-30 12:07:00');
/*!40000 ALTER TABLE `t_organizador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_participante`
--

DROP TABLE IF EXISTS `t_participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_participante` (
  `idt_participante` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `participante_nombre` varchar(100) NOT NULL,
  `participante_apellido` varchar(100) NOT NULL,
  `participante_correo` varchar(150) NOT NULL,
  `participante_clave` varchar(50) NOT NULL,
  `participante_creacion` datetime NOT NULL,
  `participante_modificacion` datetime NOT NULL,
  PRIMARY KEY (`idt_participante`),
  UNIQUE KEY `idt_participante_UNIQUE` (`idt_participante`),
  UNIQUE KEY `participante_correo_UNIQUE` (`participante_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_participante`
--

LOCK TABLES `t_participante` WRITE;
/*!40000 ALTER TABLE `t_participante` DISABLE KEYS */;
INSERT INTO `t_participante` VALUES (1,'Enrique','Iglesias','enriqueiglesias@gmail.com','8b9127934238e9a03691225c734a0a71','2012-08-01 14:32:27','2012-08-01 14:32:27'),(2,'Liliana','Palacios','lilianapalacios@gmail.com','22113d5561c167c117d38d6a2fbacf0b','2012-08-01 15:19:19','2012-08-01 15:19:19'),(3,'Daddy','Yankee','daddyankee@gmail.com','093ec71f562ba6cbf5825b7c9a48f19e','2012-10-17 22:03:25','2012-10-17 22:03:25'),(4,'Oscar','Crespo','oscarcrespo@gmail.com','f156e7995d521f30e6c59a3d6c75e1e5','2012-10-20 14:55:09','2012-10-20 14:55:09'),(5,'Larry','Page','larrypage@gmail.com','66f4b449b3a98abf87f2521e35513542','2012-10-20 15:10:01','2012-10-20 15:10:01'),(6,'Sergey','Brin','sergeybrin@gmail.com','d947f2def6d2f32c2fc7df910ed00600','2012-10-23 15:46:45','2012-10-23 15:46:45'),(7,'Pit','Bull','pitbull@gmail.com','d1e518b3bc6fe341bb2bf1a03406ec45','2012-10-24 15:55:27','2012-10-24 15:55:27'),(8,'solo','participante','soloparticipante@gmail.com','5653c6b1f51852a6351ec69c8452abc6','2012-10-30 12:06:08','2012-10-30 12:06:08');
/*!40000 ALTER TABLE `t_participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_address`
--

DROP TABLE IF EXISTS `t_shop_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_address`
--

LOCK TABLES `t_shop_address` WRITE;
/*!40000 ALTER TABLE `t_shop_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_shop_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_category`
--

DROP TABLE IF EXISTS `t_shop_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `language` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_category`
--

LOCK TABLES `t_shop_category` WRITE;
/*!40000 ALTER TABLE `t_shop_category` DISABLE KEYS */;
INSERT INTO `t_shop_category` VALUES (1,0,'Primary Articles',NULL,NULL),(2,0,'Secondary Articles',NULL,NULL),(3,1,'Red Primary Articles',NULL,NULL),(4,1,'Green Primary Articles',NULL,NULL),(5,2,'Red Secondary Articles',NULL,NULL);
/*!40000 ALTER TABLE `t_shop_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_customer`
--

DROP TABLE IF EXISTS `t_shop_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `delivery_address_id` int(11) NOT NULL,
  `billing_address_id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_customer`
--

LOCK TABLES `t_shop_customer` WRITE;
/*!40000 ALTER TABLE `t_shop_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_shop_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_image`
--

DROP TABLE IF EXISTS `t_shop_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `filename` varchar(45) NOT NULL,
  `product_id` int(11) NOT NULL,
  `entrada_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Image_Products` (`product_id`),
  KEY `fk_Image_Entrada_idx` (`entrada_id`),
  CONSTRAINT `fk_Image_Entrada` FOREIGN KEY (`entrada_id`) REFERENCES `t_entrada` (`idt_entrada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Image_Products` FOREIGN KEY (`product_id`) REFERENCES `t_shop_products` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_image`
--

LOCK TABLES `t_shop_image` WRITE;
/*!40000 ALTER TABLE `t_shop_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_shop_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_order`
--

DROP TABLE IF EXISTS `t_shop_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_order` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `delivery_address_id` int(11) DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `ordering_date` int(11) NOT NULL,
  `ordering_done` tinyint(1) DEFAULT NULL,
  `ordering_confirmed` tinyint(1) DEFAULT NULL,
  `payment_method` int(11) unsigned NOT NULL,
  `shipping_method` int(11) unsigned NOT NULL,
  `comment` text,
  `montoTotal` float(11,4) unsigned DEFAULT NULL,
  `cantidadTotal` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_customer_idx` (`customer_id`),
  KEY `fk_order_customer1` (`customer_id`),
  KEY `fk_order_paymentMethod_idx` (`payment_method`),
  KEY `fk_order_shippingMethod_idx` (`shipping_method`),
  CONSTRAINT `fk_order_customer1` FOREIGN KEY (`customer_id`) REFERENCES `t_participante` (`idt_participante`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_paymentMethod` FOREIGN KEY (`payment_method`) REFERENCES `t_shop_payment_method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_shippingMethod` FOREIGN KEY (`shipping_method`) REFERENCES `t_shop_shipping_method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_order`
--

LOCK TABLES `t_shop_order` WRITE;
/*!40000 ALTER TABLE `t_shop_order` DISABLE KEYS */;
INSERT INTO `t_shop_order` VALUES (1,3,1,1,1350747080,NULL,NULL,1,1,'',NULL,NULL),(2,3,1,1,1350747983,NULL,NULL,1,1,'',NULL,NULL),(3,3,1,1,1350749053,NULL,NULL,1,1,'',NULL,NULL),(4,3,1,1,1350750006,NULL,NULL,1,1,'',NULL,NULL),(5,3,1,1,1350750893,NULL,NULL,1,1,'',NULL,NULL),(6,3,1,1,1350750956,NULL,NULL,1,1,'',NULL,NULL),(7,3,1,1,1350751497,NULL,NULL,1,1,'',NULL,NULL),(8,3,NULL,NULL,1350757823,NULL,NULL,1,1,'',NULL,NULL),(9,3,NULL,NULL,1350758455,NULL,NULL,1,1,'',NULL,NULL),(10,3,NULL,NULL,1350758766,NULL,NULL,2,1,'',NULL,NULL),(11,5,NULL,NULL,1350778213,NULL,NULL,1,1,'',NULL,NULL),(12,5,NULL,NULL,1350778291,NULL,NULL,1,1,'',NULL,NULL),(13,5,NULL,NULL,1350780544,NULL,NULL,1,1,'',NULL,NULL),(14,5,NULL,NULL,1350781503,NULL,NULL,1,1,'',NULL,NULL),(15,5,NULL,NULL,1350783247,NULL,NULL,1,1,'',NULL,NULL),(16,5,NULL,NULL,1350784338,NULL,NULL,1,1,'',NULL,NULL),(17,4,NULL,NULL,1350869398,NULL,NULL,1,1,'',NULL,NULL),(18,4,NULL,NULL,1350924710,NULL,NULL,1,1,'',NULL,NULL),(19,4,NULL,NULL,1350926183,NULL,NULL,2,1,'',NULL,NULL),(20,4,NULL,NULL,1350932333,NULL,NULL,2,1,'',NULL,NULL),(21,4,NULL,NULL,1351021229,NULL,NULL,1,1,'',NULL,NULL),(22,4,NULL,NULL,1351107472,NULL,NULL,1,1,'',NULL,NULL),(23,7,NULL,NULL,1351108638,NULL,NULL,1,1,'',NULL,NULL),(24,4,NULL,NULL,1351280099,NULL,NULL,1,1,'',NULL,NULL),(25,4,NULL,NULL,1351280531,NULL,NULL,1,1,'Prueba con comentarios',NULL,NULL),(26,4,NULL,NULL,1351792556,NULL,NULL,1,1,'',NULL,NULL),(27,4,NULL,NULL,1352059781,NULL,NULL,4,1,'',NULL,NULL),(28,4,NULL,NULL,1352061938,NULL,NULL,4,1,'Grandes compras',NULL,NULL),(29,4,NULL,NULL,1352064661,NULL,NULL,4,1,'',NULL,NULL),(30,4,NULL,NULL,1352064762,NULL,NULL,1,1,'',NULL,NULL),(31,4,NULL,NULL,1352064957,NULL,NULL,2,1,'',NULL,NULL),(32,8,NULL,NULL,1352146262,NULL,NULL,5,1,'',NULL,NULL);
/*!40000 ALTER TABLE `t_shop_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_order_position`
--

DROP TABLE IF EXISTS `t_shop_order_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_order_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `specifications` text CHARACTER SET latin1,
  PRIMARY KEY (`id`),
  KEY `fk_order_entrada_idx` (`product_id`),
  KEY `of_orderPosition_order_idx` (`order_id`),
  CONSTRAINT `fk_orderPosition_entrada` FOREIGN KEY (`product_id`) REFERENCES `t_entrada` (`idt_entrada`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `of_orderPosition_order` FOREIGN KEY (`order_id`) REFERENCES `t_shop_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_order_position`
--

LOCK TABLES `t_shop_order_position` WRITE;
/*!40000 ALTER TABLE `t_shop_order_position` DISABLE KEYS */;
INSERT INTO `t_shop_order_position` VALUES (1,6,2,1,'null'),(2,6,9,1,'null'),(3,7,1,5,'null'),(4,7,8,10,'null'),(5,8,8,3,'null'),(6,9,9,10,'null'),(7,9,1,1,'null'),(8,10,1,1,'null'),(9,10,1,100,'null'),(10,11,10,1,'null'),(11,11,10,4,'null'),(12,12,11,10,'null'),(13,13,12,10,'null'),(14,14,13,1,'null'),(15,15,13,1,'null'),(16,16,14,1,'null'),(17,17,17,10,'null'),(18,17,16,5,'null'),(19,18,16,1,'null'),(20,18,15,1,'null'),(21,18,17,1,'null'),(22,19,19,1,'null'),(23,20,20,1,'null'),(24,21,20,100,'null'),(25,22,21,1,'null'),(26,23,17,1,'null'),(27,24,21,1,'null'),(28,24,21,1,'null'),(29,25,21,1,'null'),(30,26,21,1,'null'),(31,26,21,1,'null'),(32,26,21,1,'null'),(33,26,21,2,'null'),(34,27,22,1,'null'),(35,28,21,10,'null'),(36,28,22,15,'null'),(37,29,21,69,'null'),(38,30,22,17,'null'),(39,31,21,100,'null'),(40,32,21,1,'null');
/*!40000 ALTER TABLE `t_shop_order_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_payment_method`
--

DROP TABLE IF EXISTS `t_shop_payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_payment_method` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `tax_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_payment_method`
--

LOCK TABLES `t_shop_payment_method` WRITE;
/*!40000 ALTER TABLE `t_shop_payment_method` DISABLE KEYS */;
INSERT INTO `t_shop_payment_method` VALUES (1,'Western Union','Pago en dinero',1,0),(2,'Google Checkout','A traves de Google ',1,0),(3,'Pago contraentrega','Pagas cuando te llega',1,0),(4,'eCompras','CAINCO',1,0),(5,'Paypal','Pago a traves de Paypal',1,0);
/*!40000 ALTER TABLE `t_shop_payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_product_specification`
--

DROP TABLE IF EXISTS `t_shop_product_specification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_product_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `is_user_input` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_product_specification`
--

LOCK TABLES `t_shop_product_specification` WRITE;
/*!40000 ALTER TABLE `t_shop_product_specification` DISABLE KEYS */;
INSERT INTO `t_shop_product_specification` VALUES (1,'Size',0,1),(2,'Color',0,0),(3,'Some random attribute',0,0),(4,'Material',0,1),(5,'Specific number',1,1);
/*!40000 ALTER TABLE `t_shop_product_specification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_product_variation`
--

DROP TABLE IF EXISTS `t_shop_product_variation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_product_variation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `specification_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price_adjustion` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_product_variation`
--

LOCK TABLES `t_shop_product_variation` WRITE;
/*!40000 ALTER TABLE `t_shop_product_variation` DISABLE KEYS */;
INSERT INTO `t_shop_product_variation` VALUES (1,1,1,2,'variation1',3),(2,1,1,3,'variation2',6),(3,1,2,4,'variation3',9),(4,1,5,1,'please enter a number here',0);
/*!40000 ALTER TABLE `t_shop_product_variation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_products`
--

DROP TABLE IF EXISTS `t_shop_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `tax_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `price` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `specifications` text,
  PRIMARY KEY (`product_id`),
  KEY `fk_products_category` (`category_id`),
  CONSTRAINT `fk_products_category` FOREIGN KEY (`category_id`) REFERENCES `t_shop_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_products`
--

LOCK TABLES `t_shop_products` WRITE;
/*!40000 ALTER TABLE `t_shop_products` DISABLE KEYS */;
INSERT INTO `t_shop_products` VALUES (1,1,1,'Demonstration of Article with variations','Hello, World!','19.99',NULL,NULL),(2,1,2,'Another Demo Article with less Tax','!!','29.99',NULL,NULL),(3,2,1,'Demo3','','',NULL,NULL),(4,4,1,'Demo4','','7, 55',NULL,NULL);
/*!40000 ALTER TABLE `t_shop_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_shipping_method`
--

DROP TABLE IF EXISTS `t_shop_shipping_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_shipping_method` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `tax_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_shipping_method`
--

LOCK TABLES `t_shop_shipping_method` WRITE;
/*!40000 ALTER TABLE `t_shop_shipping_method` DISABLE KEYS */;
INSERT INTO `t_shop_shipping_method` VALUES (1,'Entrega por servicio postal','Hacemos la entrega por servicio postal. 2.99 Bs. de cargo por el servicio.',1,2.99);
/*!40000 ALTER TABLE `t_shop_shipping_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_shop_tax`
--

DROP TABLE IF EXISTS `t_shop_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_shop_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_shop_tax`
--

LOCK TABLES `t_shop_tax` WRITE;
/*!40000 ALTER TABLE `t_shop_tax` DISABLE KEYS */;
INSERT INTO `t_shop_tax` VALUES (1,'19%',19),(2,'7%',7),(3,'IVA',13);
/*!40000 ALTER TABLE `t_shop_tax` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-06 17:11:30

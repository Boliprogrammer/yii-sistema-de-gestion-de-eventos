select * from t_organizador;
select * from t_evento;
select * from t_contacto;
select * from t_entrada;
select * from t_usuario;delimiter $$
select * from person;
select * from pogo;
select * from t_campanha;
select * from t_participante;
select * from group;
select * from t_shop_category;
select * from shop_products;
select * from shop_customer;
select * from shop_order;
select * from shop_order_position;
select * from shop_product_specification;
select * from shop_product_variation;
select * from shop_payment_method;
select * from shop_order;
select * from shop_address;
select * from shop_tax;
select * from t_usuario;
select * from t_shop_image;
select * from t_entrada;

CREATE TABLE `t_tipoEntrada` (
  `idt_tipoEntrada` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipoEntrada_nombre` varchar(150) NOT NULL,
  `tipoEntrada_cantidad` float(11,4) NOT NULL,
  `tipoEntrada_habilitadoDesde` datetime DEFAULT NULL,
  `tipoEntrada_habilitadoHasta` datetime DEFAULT NULL,
  `tipoEntrada_idt_evento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_tipoEntrada`),
  UNIQUE KEY `idt_tipoentrada_UNIQUE` (`idt_tipoEntrada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$


CREATE DATABASE  IF NOT EXISTS `tesis2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tesis2`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: tesis2
-- ------------------------------------------------------
-- Server version	5.5.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_campanha`
--

DROP TABLE IF EXISTS `t_campanha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_campanha` (
  `idt_campanha` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campanha_nombre` varchar(200) NOT NULL,
  `campanha_asunto` varchar(200) NOT NULL,
  `campanha_mensaje` text,
  `campanha_idt_evento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_campanha`),
  KEY `fk_campanha_evento` (`campanha_idt_evento`),
  CONSTRAINT `fk_campanha_evento` FOREIGN KEY (`campanha_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_campanha`
--

LOCK TABLES `t_campanha` WRITE;
/*!40000 ALTER TABLE `t_campanha` DISABLE KEYS */;
INSERT INTO `t_campanha` VALUES (1,'Nombre de la campaña de alejandrobolivar86@gmail.com del evento id 8','Asunto del mensaje','',8),(2,'Nombre de la campaña','Asunto del mensaje','Mensaje',8),(3,'Nombre de la campaña','Asunto del mensaje','Mensaje',10),(4,'Campaña de Prueba 1','Asunto de la campaña de prueba 1','Mensaje',8),(5,'Campaña solo con Nombre y Asunto','Asunto','',8),(6,'Nombre de la campaña','Asunto de la campanha larga','Mensaje',15),(7,'Nombre de la campaña','Asunto','A',16),(8,'Nombre de la campaña','Asunto de la campaña','Mensaje',21),(9,'Campaña cualquiera 2','Asunto','',21),(10,'Campaña de mark Zuckerber','Asunto','',35);
/*!40000 ALTER TABLE `t_campanha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_entrada`
--

DROP TABLE IF EXISTS `t_entrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_entrada` (
  `idt_entrada` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entrada_nombre` varchar(150) NOT NULL,
  `entrada_precio` float(11,4) unsigned NOT NULL DEFAULT '0.0000',
  `entrada_cantidadMaxima` int(10) unsigned DEFAULT '0',
  `entrada_idt_evento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_entrada`),
  UNIQUE KEY `idt_entrada_UNIQUE` (`idt_entrada`),
  KEY `fk_entrada_evento` (`entrada_idt_evento`),
  CONSTRAINT `fk_entrada_evento` FOREIGN KEY (`entrada_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_entrada`
--

LOCK TABLES `t_entrada` WRITE;
/*!40000 ALTER TABLE `t_entrada` DISABLE KEYS */;
INSERT INTO `t_entrada` VALUES (1,'Estándar',100.0000,1000,27),(2,'Estándar',100.0000,100,29),(3,'VIP',300.0000,10,29),(4,'Estándar',100.0000,100,30),(5,'Butaca',100.0000,1000,31),(6,'Curva',50.0000,10000,31),(7,'Extranjeros',904.8000,1000,32),(8,'Gold',2500.9900,10,33),(9,'Standar',15.0000,100,36);
/*!40000 ALTER TABLE `t_entrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_contacto`
--

DROP TABLE IF EXISTS `t_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_contacto` (
  `idt_contacto` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `contacto_nombre` varchar(100) NOT NULL,
  `contacto_apellido` varchar(100) NOT NULL,
  `contacto_correo` varchar(150) NOT NULL,
  `contacto_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_contacto`),
  KEY `fk_contacto_organizador` (`contacto_idt_organizador`),
  CONSTRAINT `fk_contacto_organizador` FOREIGN KEY (`contacto_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contacto`
--

LOCK TABLES `t_contacto` WRITE;
/*!40000 ALTER TABLE `t_contacto` DISABLE KEYS */;
INSERT INTO `t_contacto` VALUES (1,'Carlos','Medina','carlosmedina@gmail.com',1),(3,'Lady','Gaga','ladygaga@gmail.com',1),(4,'Larry','Ellison','larryellison@gmail.com',1),(7,'Eike','Batista','eikebatista@gmail.com',1),(8,'Bill','Gates','billgates@gmail.com',5),(10,'Bon','Jovi','bonjovi@gmail.com',8),(12,'Bill','Gates','billgates@gmail.com',8),(13,'Bill','Gates','billgates@gmail.com',1),(14,'Carlos','Medina','carlosmedina@gmail.com',5),(19,'Pitbull','Pitbull','pitbull@gmail.com',4),(20,'Pitbull','Pitbull','pitbull@gmail.com',1);
/*!40000 ALTER TABLE `t_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_inscripcion`
--

DROP TABLE IF EXISTS `t_inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_inscripcion` (
  `inscripcion_idt_participante` int(10) unsigned NOT NULL,
  `inscripcion_idt_evento` int(10) unsigned NOT NULL,
  `inscripcion_fecha` datetime NOT NULL,
  PRIMARY KEY (`inscripcion_idt_participante`,`inscripcion_idt_evento`),
  KEY `fk_inscripcion_participante` (`inscripcion_idt_participante`),
  KEY `fk_inscripcion_evento` (`inscripcion_idt_evento`),
  CONSTRAINT `fk_inscripcion_participante` FOREIGN KEY (`inscripcion_idt_participante`) REFERENCES `t_participante` (`idt_participante`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_inscripcion_evento` FOREIGN KEY (`inscripcion_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_inscripcion`
--

LOCK TABLES `t_inscripcion` WRITE;
/*!40000 ALTER TABLE `t_inscripcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_compra`
--

DROP TABLE IF EXISTS `t_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_compra` (
  `idt_compra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `compra_montoTotal` float(11,4) unsigned NOT NULL,
  `compra_cantidadTotal` int(10) unsigned NOT NULL,
  `compra_fecha` datetime NOT NULL,
  `compra_idt_participante` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_compra`),
  KEY `fk_compra_participante` (`compra_idt_participante`),
  CONSTRAINT `fk_compra_participante` FOREIGN KEY (`compra_idt_participante`) REFERENCES `t_participante` (`idt_participante`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_compra`
--

LOCK TABLES `t_compra` WRITE;
/*!40000 ALTER TABLE `t_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_detallecompra`
--

DROP TABLE IF EXISTS `t_detallecompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_detallecompra` (
  `idt_detalleCompra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detalleCompra_precio` float(11,4) unsigned NOT NULL,
  `detalleCompra_cantidad` int(10) unsigned NOT NULL,
  `detalleCompra_idt_compra` int(10) unsigned NOT NULL,
  `detalleCompra_idt_entrada` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_detalleCompra`),
  KEY `fk_detalleCompra_compra` (`detalleCompra_idt_compra`),
  KEY `fk_detalleCompra_entrada` (`detalleCompra_idt_entrada`),
  CONSTRAINT `fk_detalleCompra_compra` FOREIGN KEY (`detalleCompra_idt_compra`) REFERENCES `t_compra` (`idt_compra`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleCompra_entrada` FOREIGN KEY (`detalleCompra_idt_entrada`) REFERENCES `t_entrada` (`idt_entrada`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_detallecompra`
--

LOCK TABLES `t_detallecompra` WRITE;
/*!40000 ALTER TABLE `t_detallecompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_detallecompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_evento`
--

DROP TABLE IF EXISTS `t_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_evento` (
  `idt_evento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `evento_nombre` varchar(150) NOT NULL,
  `evento_fecha_inicio` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_fecha_fin` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_tipo_evento` bit(1) NOT NULL DEFAULT b'0',
  `evento_lugar` varchar(150) DEFAULT NULL,
  `evento_cupo_maximo` int(10) unsigned DEFAULT '0',
  `evento_categoria` int(3) unsigned DEFAULT NULL,
  `evento_descripcion` text,
  `evento_estado` int(1) unsigned NOT NULL DEFAULT '0',
  `evento_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_evento`),
  KEY `fk_evento_organizador` (`evento_idt_organizador`),
  CONSTRAINT `fk_evento_organizador` FOREIGN KEY (`evento_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_evento`
--

LOCK TABLES `t_evento` WRITE;
/*!40000 ALTER TABLE `t_evento` DISABLE KEYS */;
INSERT INTO `t_evento` VALUES (2,'Proyecto de Grado I',NULL,'2012-07-20 10:06:17','','',0,4,'',1,7),(3,'Marketing II',NULL,'2012-07-21 00:00:00','','',0,0,'',0,7),(4,'Ingeniería de Software II','2012-07-20 10:10:26','2012-07-20 10:10:28','\0','',0,0,'',0,7),(5,'Congreso de Cientificos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,7),(6,'Congreso de Finanzas Personales','2012-07-20 10:22:54','2012-07-20 10:22:56','\0','',0,0,'',0,7),(7,'Congreso Bolmun','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',2000,0,'',0,7),(8,'evento de alejandrobolivar86@gmail.com','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(9,'Congreso de Psiologia','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(10,'Congresos de Informáticos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(11,'Seminario de Ventas','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(12,'Lorgio Serrate\'s Event','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(13,'Bill Gate\'s Evento','2012-07-23 10:28:53','2012-07-23 10:28:55','','Bill Gate\'s House',5000,4,'Release Windwos 8',0,1),(14,'Karl Albrecht\'\'s Event','2012-07-23 10:33:30','2012-07-23 10:33:32','\0','',0,1,'',0,1),(15,'Windows 8 Release','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',100000,0,'Especificaciones',0,5),(16,'Evento 2','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,5),(20,'asdfasd','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,18),(21,'Cata de Vino','2012-07-19 10:06:17','2012-07-28 10:06:17','\0','',0,0,'',0,19),(22,'Evento con Entradas habilitado pero sin niguna entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(23,'Evento con 2 tipos de entradas','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(24,'Evento con 1 tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(25,'Evento con 1 tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(26,'Evento con un tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(27,'Evento con un tipo de entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(28,'Evento con un tipo de entrada y con cantidad maxima obligatorio de entrada vacio','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(29,'Evento con 2 tipos de entradas y todos sus campos son obligatorios','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(30,'Prueba 1 para el caso de uso Crear Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(31,'Prueba 2 para el caso de uso Crear Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(32,'Caso de prueba 3 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(33,'Prueba 5 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(34,'Prueba 6 para el caso de uso Agregar Entrada','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,19),(35,'Evento1','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,20),(36,'Eventosss','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1);
/*!40000 ALTER TABLE `t_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_invitacion`
--

DROP TABLE IF EXISTS `t_invitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_invitacion` (
  `idt_invitacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invitacion_correos` text NOT NULL,
  `invitacion_cantidad` int(5) DEFAULT NULL,
  `invitacion_resultado` int(11) DEFAULT NULL,
  `invitacion_idt_campanha` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_invitacion`),
  KEY `fk_invitacion_campanha` (`invitacion_idt_campanha`),
  CONSTRAINT `fk_invitacion_campanha` FOREIGN KEY (`invitacion_idt_campanha`) REFERENCES `t_campanha` (`idt_campanha`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_invitacion`
--

LOCK TABLES `t_invitacion` WRITE;
/*!40000 ALTER TABLE `t_invitacion` DISABLE KEYS */;
INSERT INTO `t_invitacion` VALUES (1,'alejandrobolivar86@gmail.com',NULL,NULL,8),(2,'alejandrobolivar86@gmail.com',NULL,NULL,9),(3,'alejandrobolivar86@gmail.com, alejandrobolivar@hotmail.com',NULL,NULL,9),(4,'alejandrobolivar86@gmail.com',NULL,NULL,10),(5,'alejandrobolivar86@gmail.com',NULL,NULL,10),(6,'alejandrobolivar86@gmail.com',NULL,NULL,10),(7,'alejandrobolivar@hotmail.com',NULL,NULL,10),(8,'alejandrobolivar86@gmail.com',NULL,NULL,10),(9,'alejandrobolivar86@gmail.com',NULL,NULL,10),(10,'alejandrobolivar@hotmail.com',NULL,NULL,10);
/*!40000 ALTER TABLE `t_invitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_participante`
--

DROP TABLE IF EXISTS `t_participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_participante` (
  `idt_participante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participante_nombre` varchar(100) NOT NULL,
  `participante_apellido` varchar(100) NOT NULL,
  `participante_correo` varchar(150) NOT NULL,
  `participante_clave` varchar(50) NOT NULL,
  `participante_creacion` datetime NOT NULL,
  `participante_modificacion` datetime NOT NULL,
  PRIMARY KEY (`idt_participante`),
  UNIQUE KEY `idt_participante_UNIQUE` (`idt_participante`),
  UNIQUE KEY `participante_correo_UNIQUE` (`participante_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_participante`
--

LOCK TABLES `t_participante` WRITE;
/*!40000 ALTER TABLE `t_participante` DISABLE KEYS */;
INSERT INTO `t_participante` VALUES (1,'Enrique','Iglesias','enriqueiglesias@gmail.com','8b9127934238e9a03691225c734a0a71','2012-08-01 14:32:27','2012-08-01 14:32:27'),(2,'Liliana','Palacios','lilianapalacios@gmail.com','22113d5561c167c117d38d6a2fbacf0b','2012-08-01 15:19:19','2012-08-01 15:19:19');
/*!40000 ALTER TABLE `t_participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_organizador`
--

DROP TABLE IF EXISTS `t_organizador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_organizador` (
  `idt_organizador` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organizador_nombre` varchar(100) NOT NULL,
  `organizador_apellido` varchar(100) NOT NULL,
  `organizador_empresa` varchar(150) DEFAULT NULL,
  `organizador_correo` varchar(150) NOT NULL,
  `organizador_clave` varchar(50) NOT NULL,
  `organizador_creacion` datetime NOT NULL,
  `organizador_modificacion` datetime NOT NULL,
  PRIMARY KEY (`idt_organizador`),
  UNIQUE KEY `organizador_correo_UNIQUE` (`organizador_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_organizador`
--

LOCK TABLES `t_organizador` WRITE;
/*!40000 ALTER TABLE `t_organizador` DISABLE KEYS */;
INSERT INTO `t_organizador` VALUES (1,'Alejandro','Bolivar','OPC Eventos Intelectuales Operativos','alejandrobolivar86@gmail.com','e052450f29b2e0e9a53fd4eb389e25a9','2012-07-16 15:19:04','2012-07-23 19:58:48'),(2,'Made','Jacobs','UPSA','madejacobs@gmail.com','made','2012-07-16 15:52:45','2012-07-16 15:52:45'),(3,'Fernando','Chavez','YPFB','fernandochavez@gmail.com','cebdd715d4ecaafee8f147c2e85e0754','2012-07-16 16:54:06','2012-07-16 16:54:06'),(4,'Carlos','Slim','Telecom','carlosslim@gmail.com','dc599a9972fde3045dab59dbd1ae170b','2012-07-17 14:37:08','2012-07-17 14:37:08'),(5,'Bill','Gates','Microsoft','billgates@gmail.com','e8375d7cd983efcbf956da5937050ffc','2012-07-17 14:37:50','2012-07-23 20:45:46'),(7,'Warren','Buffett','Berkshire Hathaway','warrenbuffett@gmail.com','80ee7ece5bdf991bc2ae95776f02568d','2012-07-19 17:38:57','2012-07-19 17:38:57'),(8,'Lorgio','Serrate','Muebles Metálicos Bolivar','lorgioserrate@gmail.com','cbe32f85911a867690f8324cd91737c2','2012-07-20 18:38:56','2012-07-23 18:56:23'),(18,'Michael','Mind','','michaelmind@gmail.com','0acf4539a14b3aa27deeb4cbdf6e989f','2012-07-26 15:17:37','2012-07-26 15:17:37'),(19,'Oscar','Crespo','','oscarcrespo@gmail.com','f156e7995d521f30e6c59a3d6c75e1e5','2012-07-28 17:15:58','2012-07-28 17:15:58'),(20,'Mark','Zuckerberg','','markzuckerberg@gmail.com','ea82410c7a9991816b5eeeebe195e20a','2012-07-31 12:56:38','2012-07-31 12:56:38'),(21,'Juan','Magan','','juanmagan@gmail.com','a94652aa97c7211ba8954dd15a3cf838','2012-08-01 11:45:52','2012-08-01 11:45:52');
/*!40000 ALTER TABLE `t_organizador` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-04  0:15:05

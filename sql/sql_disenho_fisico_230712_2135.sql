CREATE DATABASE  IF NOT EXISTS `tesis2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tesis2`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: tesis2
-- ------------------------------------------------------
-- Server version	5.5.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_contacto`
--

DROP TABLE IF EXISTS `t_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_contacto` (
  `idt_contacto` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `contacto_nombre` varchar(100) NOT NULL,
  `contacto_apellido` varchar(100) NOT NULL,
  `contacto_correo` varchar(150) NOT NULL,
  `contacto_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_contacto`),
  KEY `fk_contacto_organizador` (`contacto_idt_organizador`),
  CONSTRAINT `fk_contacto_organizador` FOREIGN KEY (`contacto_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_contacto`
--

LOCK TABLES `t_contacto` WRITE;
/*!40000 ALTER TABLE `t_contacto` DISABLE KEYS */;
INSERT INTO `t_contacto` VALUES (1,'Carlos','Medina','carlosmedina@gmail.com',1),(3,'Lady','Gaga','ladygaga@gmail.com',1),(4,'Larry','Ellison','larryellison@gmail.com',1),(7,'Eike','Batista','eikebatista@gmail.com',1),(8,'Bill','Gates','billgates@gmail.com',5),(9,'Bill','Gates','billgates@gmail.com',6),(10,'Bon','Jovi','bonjovi@gmail.com',8),(12,'Bill','Gates','billgates@gmail.com',8),(13,'Bill','Gates','billgates@gmail.com',1);
/*!40000 ALTER TABLE `t_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_evento`
--

DROP TABLE IF EXISTS `t_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_evento` (
  `idt_evento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `evento_nombre` varchar(150) NOT NULL,
  `evento_fecha_inicio` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_fecha_fin` datetime DEFAULT '2012-07-20 10:06:17',
  `evento_tipo_evento` bit(1) NOT NULL DEFAULT b'0',
  `evento_lugar` varchar(150) DEFAULT NULL,
  `evento_cupo_maximo` int(10) unsigned DEFAULT '0',
  `evento_categoria` int(3) unsigned DEFAULT NULL,
  `evento_descripcion` text,
  `evento_estado` int(1) unsigned NOT NULL DEFAULT '0',
  `evento_idt_organizador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_evento`),
  KEY `fk_evento_organizador` (`evento_idt_organizador`),
  CONSTRAINT `fk_evento_organizador` FOREIGN KEY (`evento_idt_organizador`) REFERENCES `t_organizador` (`idt_organizador`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_evento`
--

LOCK TABLES `t_evento` WRITE;
/*!40000 ALTER TABLE `t_evento` DISABLE KEYS */;
INSERT INTO `t_evento` VALUES (2,'Proyecto de Grado I',NULL,'2012-07-20 10:06:17','','',0,4,'',1,7),(3,'Marketing II',NULL,'2012-07-21 00:00:00','','',0,0,'',0,7),(4,'Ingeniería de Software II','2012-07-20 10:10:26','2012-07-20 10:10:28','\0','',0,0,'',0,7),(5,'Congreso de Cientificos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,7),(6,'Congreso de Finanzas Personales','2012-07-20 10:22:54','2012-07-20 10:22:56','\0','',0,0,'',0,7),(7,'Congreso Bolmun','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',2000,0,'',0,7),(8,'evento de alejandrobolivar86@gmail.com','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(9,'Congreso de Psiologia','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,1),(10,'Congresos de Informáticos','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(11,'Seminario de Ventas','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(12,'Lorgio Serrate\'s Event','2012-07-20 10:06:17','2012-07-20 10:06:17','\0','',0,0,'',0,8),(13,'Bill Gate\'s Evento','2012-07-23 10:28:53','2012-07-23 10:28:55','','Bill Gate\'s House',5000,4,'Release Windwos 8',0,1),(14,'Karl Albrecht\'\'s Event','2012-07-23 10:33:30','2012-07-23 10:33:32','\0','',0,1,'',0,1);
/*!40000 ALTER TABLE `t_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_organizador`
--

DROP TABLE IF EXISTS `t_organizador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_organizador` (
  `idt_organizador` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organizador_nombre` varchar(100) NOT NULL,
  `organizador_apellido` varchar(100) NOT NULL,
  `organizador_empresa` varchar(150) DEFAULT NULL,
  `organizador_correo` varchar(150) NOT NULL,
  `organizador_clave` varchar(50) NOT NULL,
  `organizador_creacion` datetime NOT NULL,
  `organizador_modificacion` datetime NOT NULL,
  PRIMARY KEY (`idt_organizador`),
  UNIQUE KEY `organizador_correo_UNIQUE` (`organizador_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_organizador`
--

LOCK TABLES `t_organizador` WRITE;
/*!40000 ALTER TABLE `t_organizador` DISABLE KEYS */;
INSERT INTO `t_organizador` VALUES (1,'Alejandro','Bolivar','OPC Eventos Intelectuales Operativos','alejandrobolivar86@gmail.com','e052450f29b2e0e9a53fd4eb389e25a9','2012-07-16 15:19:04','2012-07-23 19:58:48'),(2,'Made','Jacobs','UPSA','madejacobs@gmail.com','made','2012-07-16 15:52:45','2012-07-16 15:52:45'),(3,'Fernando','Chavez','YPFB','fernandochavez@gmail.com','cebdd715d4ecaafee8f147c2e85e0754','2012-07-16 16:54:06','2012-07-16 16:54:06'),(4,'Carlos','Slim','Telecom','carlosslim@gmail.com','dc599a9972fde3045dab59dbd1ae170b','2012-07-17 14:37:08','2012-07-17 14:37:08'),(5,'Bill','Gates','Microsoft','billgates@gmail.com','e8375d7cd983efcbf956da5937050ffc','2012-07-17 14:37:50','2012-07-23 20:45:46'),(6,'Warren','Buffet','Berkshite Hathaway','warrenbuffet@gmail.com','80ee7ece5bdf991bc2ae95776f02568d','2012-07-19 08:25:52','2012-07-19 08:25:52'),(7,'Warren','Buffett','Berkshire Hathaway','warrenbuffett@gmail.com','80ee7ece5bdf991bc2ae95776f02568d','2012-07-19 17:38:57','2012-07-19 17:38:57'),(8,'Lorgio','Serrate','Muebles Metálicos Bolivar','lorgioserrate@gmail.com','cbe32f85911a867690f8324cd91737c2','2012-07-20 18:38:56','2012-07-23 18:56:23');
/*!40000 ALTER TABLE `t_organizador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_campanha`
--

DROP TABLE IF EXISTS `t_campanha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_campanha` (
  `idt_campanha` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campanha_nombre` varchar(200) NOT NULL,
  `campanha_asunto` varchar(200) NOT NULL,
  `campanha_mensaje` text,
  `campanha_idt_evento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idt_campanha`),
  KEY `fk_campanha_evento` (`campanha_idt_evento`),
  CONSTRAINT `fk_campanha_evento` FOREIGN KEY (`campanha_idt_evento`) REFERENCES `t_evento` (`idt_evento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_campanha`
--

LOCK TABLES `t_campanha` WRITE;
/*!40000 ALTER TABLE `t_campanha` DISABLE KEYS */;
INSERT INTO `t_campanha` VALUES (1,'Nombre de la campaña de alejandrobolivar86@gmail.com del evento id 8','Asunto del mensaje','',8),(2,'Nombre de la campaña','Asunto del mensaje','Mensaje',8),(3,'Nombre de la campaña','Asunto del mensaje','Mensaje',10),(4,'Campaña de Prueba 1','Asunto de la campaña de prueba 1','Mensaje',8),(5,'Campaña solo con Nombre y Asunto','Asunto','',8);
/*!40000 ALTER TABLE `t_campanha` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-07-23 21:35:49
